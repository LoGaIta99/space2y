
clear
close

function dx = f( t , x )

//Features of the missile
M0 = 300; //kg
Is = 250; //s
T = 10000; // N

D = 0.35; //Missile diameter, m
L = 7.0 ; //Missile length, m
Sref = %pi * ( D / 2 )^2; //Reference surface area, m2


Xcg = 1.5; //Center of gravity from nose, m
Xcp = 0.5; //Center of pressure from nose, m  
Xf  = 3.0; //Position of CP for wings
Lf  = 0.3; //Side size of the squared wing

Sreffins = 2 * Lf ^2;

//Control law of fins
// 0 - 1s : deltaF = 0;
// 1 - 2s : deltaF 0 -> -0.3;
// 2 - 3s : deltaF = -0.3;
// 3 - 4s : deltaF -0.3 -> 0;

maxdelta = -0.3;

if t < 1 then
    deltaF = 0;
elseif ( t < 2 )
    deltaF = maxdelta * ( t - 1 );
elseif ( t < 3 )
    deltaF = maxdelta;
elseif ( t < 4 )
    deltaF = maxdelta - maxdelta * ( t - 3 );
else
    deltaF = 0;
end




Cd = 0.5; //Drag coefficient
dCnda = 2 .* %pi; //Derivative of the normal force coefficient w.r.t. alpha
dCndafin = 2 .* %pi; //Derivative of the normal force coefficient w.r.t. alpha
delta = 0; // Thrust is aligned with the body axis


//constants
g0 = 9.81; // m/s2
rho= 1.25; // density, kg/m3

sqVeff = x( 2 )^2 + x( 4 )^2;
q = 0.5 * rho * sqVeff;

gam = atan( x( 4 ) / x( 2 ) ); //Flight path angle
alpha = x( 5 ) - gam; //Alpha derived from phi = x(5)
alphafins = alpha + deltaF;

Cn = dCnda * alpha;
Cnfin = dCndafin * alphafins;

N = q * Sref * Cn; // Normal force
A = q * Sref * Cd; // Axial force
Nf = q * Sreffins * Cnfin;

m = M0 - t * T / g0 / Is
Izz = 1 / 12 * m * L^2; 

Fx = T * cos( x( 5 ) + delta ) - A * cos( x( 5 ) ) - N * sin( x( 5 ) );
Fy = T * sin( x( 5 ) + delta ) - A * sin( x( 5 ) ) + N * cos( x( 5 ) ) - m * g0; 
Mzz = -N * ( Xcp - Xcg ) - Nf * cos( deltaF ) * ( Xf - Xcg );


dx( 1 ) = x( 2 );
dx( 2 ) = Fx / m;

dx( 3 ) = x( 4 );
dx( 4 ) = Fy / m;

dx( 5 ) = x( 6 );
dx( 6 ) = Mzz / Izz;

endfunction












//Initial conditions
x0( 1 ) = 0; //Starting position, m
x0( 2 ) = 10; // Initial velocity x, m/s
x0( 3 ) = 0; //Starting position, m 
x0( 4 ) = 10; // Initial velocity y, m/s
x0( 5 ) = 0; // Initial angle y, rad
x0( 6 ) = 0; // Initial angular velocity y, rad/s



t0 = 0;

t = 0 : 0.01 : 10;

y = ode( x0 , t0 , t , f ); 





