close all
clear all
clc

N = 4; % Test number

for i = 1:N
    DATA = csvread(sprintf('fire_%i.csv', i));
    
    varname1 = genvarname(sprintf('time_%i', i));
    varname2 = genvarname(sprintf('thrust_%i', i));

    
    eval(sprintf('time_%i = DATA(:,1);', i));
    eval(sprintf('thrust_%i = DATA(:,2);', i));
end


h = figure;
set(h, 'Units', 'normalized', 'OuterPosition', [0.15 0.6 0.70 0.5]);
grid on
hold on

for i = 1:N
    
    time = eval(sprintf('time_%i', i));
    time = time - time(1);
    thrust = eval(sprintf('thrust_%i', i));
    thrust = thrust * 0.00981; % Gram to Newton
    
    plot(time, thrust)
    xlabel('t [ms]');    ylabel('T [N]');
end

legend('test_1', 'test_2', 'test_3', 'test_4')



h = figure;
set(h, 'Units', 'normalized', 'OuterPosition', [0.15 0.00 0.70 0.5]);
grid on
hold on
time = eval(sprintf('time_%i', 1));
time = time - time(1);
thrust = eval(sprintf('thrust_%i', 1));
thrust = thrust * 0.00981;

upper = thrust + .2*std(thrust);
lower = thrust - .2*std(thrust); lower(lower<0) = 0;
plot(time, lower)
plot(time, upper)

time = eval(sprintf('time_%i', 4));
time = time - time(1);
thrust = eval(sprintf('thrust_%i', 4));
thrust = thrust * 0.00981;
plot(time, thrust)
xlabel('t [ms]');    ylabel('T [N]');



