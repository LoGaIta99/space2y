function [fval] = fun_peaks_penalty(x,constant)

fval = constant*(1-x(1)).^2.*exp(-(x(1).^2) - (x(2)+1).^2) - 10*(x(1)/5 - x(1).^3 - x(2).^5).*exp(-x(1).^2-x(2).^2)  - 1/3*exp(-(x(1)+1).^2 - x(2).^2);

[c,ceq] = nonlinconstr(x);

constraint_violation = 0;
if ~isempty(c)
    if c > 0
        constraint_violation = c;
    end
end
% The constraint violation corresponds to the drift from the unit circle

if ~isempty(ceq)
    constraint_violation = constraint_violation + abs(ceq);
end


violationWeight = 100;
fval = fval + violationWeight*constraint_violation;

end

