\begin{thenomenclature} 

 \nomgroup{A}

  \item [{$\gamma$}]\begingroup Interferometric coherence \nomunit{-}\nomeqref {0}
		\nompageref{i}
  \item [{$\sigma$}]\begingroup Backscattering coefficient \nomunit{dB}\nomeqref {0}
		\nompageref{i}
  \item [{$\snr$}]\begingroup Signal to Noise Ratio \nomunit{dB}\nomeqref {0}
		\nompageref{i}
  \item [{$f_\mathrm{dir}$}]\begingroup Antenna directional function \nomunit{-}\nomeqref {0}
		\nompageref{i}
  \item [{$G$}]\begingroup Antenna gain \nomunit{dB}\nomeqref {0}
		\nompageref{i}
  \item [{$I$}]\begingroup Pixel value \nomunit{-}\nomeqref {0}
		\nompageref{i}
  \item [{$L$}]\begingroup Length of the averaging window \nomunit{-}\nomeqref {0}
		\nompageref{i}
  \item [{$L_\mathrm{atm}$}]\begingroup Atmospheric loss \nomunit{dB}\nomeqref {0}
		\nompageref{i}
  \item [{$N_\mathrm{eq}$}]\begingroup No. equivalent independent samples \nomunit{-}\nomeqref {0}
		\nompageref{i}
  \item [{$p$}]\begingroup AGB fitting exponent \nomunit{-}\nomeqref {0}
		\nompageref{i}
  \item [{$u$}]\begingroup Combined uncertainty \nomunit{-}\nomeqref {0}
		\nompageref{i}

 \nomgroup{B}

  \item [{$\Delta \Psi$}]\begingroup Antenna azimuthal beamwidth \nomunit{deg}\nomeqref {0}
		\nompageref{i}
  \item [{$\Delta x_\mathrm{ant}$}]\begingroup Spatial sampling of synthetic aperture \nomunit{m}\nomeqref {0}
		\nompageref{i}
  \item [{$\lambda$}]\begingroup Wavelength \nomunit{m}\nomeqref {0}
		\nompageref{i}
  \item [{$\mathrm{AGB}$}]\begingroup Above Ground Biomass \nomunit{\frac{ton}{hect}}\nomeqref {0}
		\nompageref{i}
  \item [{$\mu_E$}]\begingroup Earth's gravitational parameter \nomunit{\frac{km^3}{s^2}}\nomeqref {0}
		\nompageref{i}
  \item [{$\phi$}]\begingroup Interferometric phase \nomunit{deg}\nomeqref {0}
		\nompageref{i}
  \item [{$\rho_R$}]\begingroup Range resolution \nomunit{m}\nomeqref {0}
		\nompageref{i}
  \item [{$\rho_x$}]\begingroup Azimuth resolution \nomunit{m}\nomeqref {0}
		\nompageref{i}
  \item [{$\tau$}]\begingroup Slow time \nomunit{s}\nomeqref {0}
		\nompageref{i}
  \item [{$\theta$}]\begingroup Incidence angle (off-nadir) \nomunit{deg}\nomeqref {0}
		\nompageref{i}
  \item [{$A$}]\begingroup AGB fitting coefficient \nomunit{\frac{hect}{ton}}\nomeqref {0}
		\nompageref{i}
  \item [{$A_\mathrm{ant}$}]\begingroup Antenna area \nomunit{m^2}\nomeqref {0}
		\nompageref{i}
  \item [{$A_\mathrm{ill}$}]\begingroup Illuminated area \nomunit{hect}\nomeqref {0}
		\nompageref{i}
  \item [{$A_s$}]\begingroup Synthetic aperture length \nomunit{m}\nomeqref {0}
		\nompageref{i}
  \item [{$B$}]\begingroup Bandwidth \nomunit{MHz}\nomeqref {0}
		\nompageref{i}
  \item [{$B_n$}]\begingroup Normal baseline \nomunit{m}\nomeqref {0}
		\nompageref{i}
  \item [{$c$}]\begingroup Speed of light in vacuum \nomunit{\frac{m}{s}}\nomeqref {0}
		\nompageref{i}
  \item [{$E_r$}]\begingroup Received energy \nomunit{J}\nomeqref {0}
		\nompageref{i}
  \item [{$f$}]\begingroup Frequency \nomunit{MHz}\nomeqref {0}
		\nompageref{i}
  \item [{$f_0$}]\begingroup Carrier frequency \nomunit{MHz}\nomeqref {0}
		\nompageref{i}
  \item [{$H$}]\begingroup Orbital height \nomunit{km}\nomeqref {0}
		\nompageref{i}
  \item [{$L_x$}]\begingroup Antenna azimuth-wise length \nomunit{m}\nomeqref {0}
		\nompageref{i}
  \item [{$L_z$}]\begingroup Antenna zenith-wise length \nomunit{m}\nomeqref {0}
		\nompageref{i}
  \item [{$N_0$}]\begingroup Noise power density \nomunit{J}\nomeqref {0}
		\nompageref{i}
  \item [{$P_r$}]\begingroup Received power \nomunit{W}\nomeqref {0}
		\nompageref{i}
  \item [{$P_t$}]\begingroup Transmitted power \nomunit{W}\nomeqref {0}
		\nompageref{i}
  \item [{$R$}]\begingroup Slant range \nomunit{km}\nomeqref {0}
		\nompageref{i}
  \item [{$R_E$}]\begingroup Earth's equatorial radius \nomunit{km}\nomeqref {0}
		\nompageref{i}
  \item [{$s$}]\begingroup Slant range displacement \nomunit{m}\nomeqref {0}
		\nompageref{i}
  \item [{$t$}]\begingroup Fast time \nomunit{s}\nomeqref {0}
		\nompageref{i}
  \item [{$T_\mathrm{avg}$}]\begingroup Duration of the averaging window \nomunit{\mu s}\nomeqref {0}
		\nompageref{i}
  \item [{$T_\mathrm{obs}$}]\begingroup Pulse duration \nomunit{\mu s}\nomeqref {0}
		\nompageref{i}
  \item [{$T_n$}]\begingroup Noise temperature \nomunit{K}\nomeqref {0}
		\nompageref{i}
  \item [{$v_\mathrm{orb}$}]\begingroup Orbital velocity \nomunit{m/s}\nomeqref {0}
		\nompageref{i}
  \item [{$z$}]\begingroup Terrain topography \nomunit{m}\nomeqref {0}
		\nompageref{i}
  \item [{$z_\mathrm{amb}$}]\begingroup Height of ambiguity \nomunit{m}\nomeqref {0}
		\nompageref{i}
  \item [{$z_s$}]\begingroup Cell distance normal to slant range \nomunit{m}\nomeqref {0}
		\nompageref{i}

 \nomgroup{D}

  \item [{\acron{AGB}{Above Ground Biomass}}]\begingroup \nomeqref {0}
		\nompageref{i}
  \item [{\acron{CRLB}{Cramer-Rao Lower Bound}}]\begingroup \nomeqref {0}
		\nompageref{i}
  \item [{\acron{PRI}{Pulse Repetition Interval}}]\begingroup \nomeqref {0}
		\nompageref{i}
  \item [{\acron{RCS}{Radar Cross-Section}}]\begingroup \nomeqref {0}
		\nompageref{i}
  \item [{\acron{RSS}{Root of Sum of Squares}}]\begingroup \nomeqref {0}
		\nompageref{i}
  \item [{\acron{SAR}{Synthetic Aperture Radar}}]\begingroup \nomeqref {0}
		\nompageref{i}
  \item [{\acron{SNR}{Signal to Noise Ratio}}]\begingroup \nomeqref {0}
		\nompageref{i}
  \item [{\acron{TDBP}{Time Domain Back-Projection}}]\begingroup \nomeqref {0}
		\nompageref{i}
  \item [{\acron{w.r.t.}{with respect to}}]\begingroup \nomeqref {0}
		\nompageref{i}

\end{thenomenclature}
