\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {english}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1}Estimation in SAR Interferometry}{1}{section.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.1}Introduction}{1}{subsection.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.2}Estimation of Topography and Displacement-Rate}{2}{subsection.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.3}Estimation Performance}{3}{subsection.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Theoretical Evaluation}{3}{subsection.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Empirical Evaluation}{3}{equation.1.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2}Radar Target Detection}{6}{section.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1}Target Detection}{7}{subsection.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.1.1}Detection Based on the Knowledge of $x(t)$ and $y(t)$}{7}{subsubsection.2.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.1.2}Detection Based on the Knowledge of $y(t)$ Only}{9}{subsubsection.2.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2}Effect of Reducing the Processed Data Volume}{9}{subsection.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.2.1}Detection Based on the Knowledge of $x(t)$ and $y(t)$}{10}{subsubsection.2.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.2.2}Detection Based on the Knowledge of $y(t)$ Only}{10}{subsubsection.2.2.2}
