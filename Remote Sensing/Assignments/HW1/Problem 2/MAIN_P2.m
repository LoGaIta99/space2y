%% ASSIGNEMNT 1
% 2ND PROBLEM

close all
clear all
clc

load HW_1_Problem_2.mat 
% y(t): received signal
% x(t): diret signal
% dt: sampling interval
% x = x(1:800);
% y = y(1:800);

set(0,'defaultTextInterpreter','latex')
set(0,'DefaultAxesFontSize', 16);


Nt = length(x);
t = dt * [0:(Nt-1)];
Tobs = t(end);

%% 0. Spectral Analysis

% Estimation of the Power Spectral Density
% FT
Nf = 2 * 2^ceil(log2(Nt));
%Nf = 2*Nf

% INPUT SIGNAL x(t)
[X,f] = dft(transpose(x),t,Nf);
% transpose is necessary since dft works along columns!

X = X'; % realizations, frequency
X = X*dt; % scale factor for dimensional consistentcy 
% Power Spectral Density
Sx = 1/Tobs*abs(X).^2;


N_sub = 2000;
if length(x) < N_sub
    N_sub = length(x);
end
h = figure;
subplot(2,2,1)
plot(t(1:N_sub), real(x(1:N_sub)))
hold on
plot(t(1:N_sub), imag(x(1:N_sub)))
xlabel('time [s]')
legend('Re$\{x\}$', 'Im$\{x\}$', 'Interpreter', 'latex')
title(sprintf('x(t) (first %i samples)', N_sub))

subplot(2,2,3)
plot(f,Sx)
grid on
xlabel('frequency [Hz]')
title('$S_{x}(f)$')


% RECEIVED SIGNAL y(t)
[Y,f] = dft(transpose(y),t,Nf);
Y = Y'; % realizations, frequency
Y = Y*dt; % scale factor for dimensional consistentcy 
Sy = 1/Tobs*abs(Y).^2;    

subplot(2,2,2)
plot(t(1:N_sub), real(y(1:N_sub)))
hold on
plot(t(1:N_sub), imag(y(1:N_sub)))
xlabel('time [s]')
legend('Re$\{y\}$', 'Im$\{y\}$', 'Interpreter', 'latex')
title(sprintf('y(t) (first %i samples)', N_sub))

subplot(2,2,4)
plot(f,Sy)
grid on
xlabel('frequency [Hz]')
title('$S_{y}(f)$')

fileName = 'directAndReceivedSignals';
set(h, 'Units', 'normalized', 'OuterPosition', [0.13 0.3 0.74 0.6]);
h.Renderer = 'Painter';
printFigure(h, fileName)

std_x = std(x)
std_y = std(y)


%% 1.
% Propose an algorithm to detect the number of ground targets and estimate
% the associated parameters tau_p and A_p assuming that the observed data
% includes both y(t) and x(t).


[X,f] = dft(transpose(x),t,Nf);
[Y,f] = dft(transpose(y),t,Nf);
Syx = 1/Tobs*Y.*conj(X); % cross-spectrum 
Ryx = real( idft(Syx,f,t) ); % Any imaginary part is simply due to numerical errors

cxx0 = sum(abs(x).^2);
cyy0 = sum(abs(y).^2);
scaleCoeffCross = sqrt(cxx0*cyy0);
Ryx = Ryx./scaleCoeffCross;


% Ryx_pm = xcorr(y, x, 'normalized'); % Cross-correlation
% idx_half = floor(size(Ryx_pm)/2); idx_half = idx_half(1)+1;
% Ryx = real(Ryx_pm(idx_half:end));
h = figure;
subplot(2,1,1)
plot(t, Ryx)
grid on
idx_zoom_in = 150;
idx_zoom_fin = 400;
linesCMap = lines(10);
rectangle('Position',[t(idx_zoom_in) min(Ryx) (t(idx_zoom_fin)-t(idx_zoom_in)) (max(Ryx)-min(Ryx))],...
    'Curvature',0, 'EdgeColor', linesCMap(2,:), 'LineWidth', 2)
xlabel('time lag [s]'), ylabel('$R_{yx}(\tau)$')
title('Scaled Cross-correlation')

subplot(2,1,2)
stem(t(idx_zoom_in:idx_zoom_fin), real(Ryx(idx_zoom_in:idx_zoom_fin)))
grid on
hold on
[Rxy_sort, idx_sort] = sort( abs(Ryx(idx_zoom_in+1:end)), 'descend' );
idx_sort = idx_zoom_in+idx_sort;
tau_sort = t(idx_sort);
No_peaks = 3;
Rxy_peaks = Rxy_sort(1:No_peaks); % Identifies the 3 peaks
tau_peaks = tau_sort(1:No_peaks);
plot(tau_peaks, Rxy_peaks, '.', 'MarkerSize', 15, 'MarkerFaceColor', linesCMap(2,:))

for k = 1:length(tau_peaks)
    text(1.007*tau_peaks(k), 0.95*Rxy_peaks(k), sprintf('%.3f \n $\\mu$s', tau_peaks(k)*1e+6),...
        'FontSize', 13, 'Color', linesCMap(2,:), 'Interpreter', 'latex')
end

xlabel('time lag [s]'), ylabel('$R_{yx}(\tau)$')
title('Zoom')

fileName = 'cross_correl_xy';
set(h, 'Units', 'normalized', 'OuterPosition', [0.13 0.3 0.74 0.6]);
h.Renderer = 'Painter';
printFigure(h, fileName)


n_at_peaks = round( tau_peaks/dt ) + 1; % round() to prevent numerical errors
x_peaks = x(n_at_peaks);
y_peaks = y(n_at_peaks);





%%
% 1. (Alternative method)

echo = idft((Y./X-1), f, t);
echo = real(echo);
h = figure;

subplot(2,1,1)
plot(t, echo)
grid on
hold on
idx_zoom_in = 150;
idx_zoom_fin = 400;
rectangle('Position',[t(idx_zoom_in) min(echo) (t(idx_zoom_fin)-t(idx_zoom_in)) (max(echo)-min(echo))],...
    'Curvature',0, 'EdgeColor', linesCMap(2,:), 'LineWidth', 2)
xlabel('time [s]'), ylabel('$e(t)$')
title('Echo from scattering targets')


subplot(2,1,2)
stem(t(idx_zoom_in:idx_zoom_fin), echo(idx_zoom_in:idx_zoom_fin))
grid on
hold on
[echo_sort, idx_sort] = sort( abs(echo(idx_zoom_in+1:end)), 'descend' );
idx_sort = idx_zoom_in+idx_sort;
tau_sort = t(idx_sort);
No_peaks = 3;
echo_peaks = echo_sort(1:No_peaks); % Identifies the 3 peaks
tau_peaks = tau_sort(1:No_peaks);
plot(tau_peaks, echo_peaks, '.', 'MarkerSize', 15, 'MarkerFaceColor', linesCMap(2,:))

for k = 1:length(tau_peaks)
    text(1.007*tau_peaks(k), 0.95*echo_peaks(k), sprintf('%.3f', echo_peaks(k)),...
        'FontSize', 13, 'Color', linesCMap(2,:), 'Interpreter', 'latex')
end

xlabel('time [s]'), ylabel('$e(t)$')
title('Zoom')

fileName = 'echo_time';
set(h, 'Units', 'normalized', 'OuterPosition', [0.13 0.3 0.74 0.6]);
h.Renderer = 'Painter';
printFigure(h, fileName)




%% 2.-3.
% 2.
% Repeat point 1 assuming that the observed data includes only y(t).
% 3. 
% Apply the estimation algorithm on the data-set, observe the results and
% discuss them.

n_max = length(x);

% Autocorrelation by inverse FT
Ry = idft(transpose(Sy),f,t); 
% transpose is necessary since idft works along columns
Ry = transpose(Ry); % realizations, delta-time
% check: Rx should be real since x is a real-valued process
max_err_imag = max(abs(imag(Ry(:))))
Ry = real(Ry);

% scale factor for dimensional consistentcy 
% (!!!: idft is based on built-in function ifft, that divides the result by
% the number of frequency points
df = f(2)-f(1);
Ry = Ry*df*Nf; 
%

Ry_avg_temp = Ry;


Ry = mean(Ry_avg_temp,1); % average over realizations 

h = figure;
subplot(2,1,1)
plot(t, Ry)
grid on
idx_zoom_in = 150;
idx_zoom_fin = 400;
linesCMap = lines(10);
rectangle('Position',[t(idx_zoom_in) min(Ry) (t(idx_zoom_fin)-t(idx_zoom_in)) (max(Ry)-min(Ry))],...
    'Curvature',0, 'EdgeColor', linesCMap(2,:), 'LineWidth', 2)
xlabel('time lag [s]'), ylabel('$R_{yy}(\tau)$')
title('Auto-correlation')

subplot(2,1,2)
stem(t(idx_zoom_in:idx_zoom_fin), real(Ry(idx_zoom_in:idx_zoom_fin)))
grid on
hold on
[Ry_sort, idx_sort] = sort( abs(Ry(idx_zoom_in+1:end)), 'descend' );
idx_sort = idx_zoom_in+idx_sort;
tau_sort = t(idx_sort);
No_peaks = 3;
Ry_peaks = Ry_sort(1:No_peaks); % Identifies the 3 peaks
tau_peaks = tau_sort(1:No_peaks);
plot(tau_peaks, Ry_peaks, '.', 'MarkerSize', 15, 'MarkerFaceColor', linesCMap(2,:))

for k = 1:length(tau_peaks)
    text(1.006*tau_peaks(k), 0.95*Ry_peaks(k), sprintf('%.3f \n $\\mu$s', tau_peaks(k)*1e+6),...
        'FontSize', 13, 'Color', linesCMap(2,:), 'Interpreter', 'latex')
end

xlabel('time lag [s]'), ylabel('$R_{yy}(\tau)$')
title('Zoom')

fileName = 'auto_correl_yy';
set(h, 'Units', 'normalized', 'OuterPosition', [0.13 0.3 0.74 0.6]);
h.Renderer = 'Painter';
printFigure(h, fileName)



%% 4. 
% What is the minimum temporal separation tau_2-tau_1 required to resolve
% two targets? Note: two targets are said to be resolved if they appear as
% two distinct peaks.


%% 5. 
% As an effort to reduce the data volume, both y(t) and x(t) are shortened
% by retaining only the first 800 samples. What is the impact of this
% choice on the results?




