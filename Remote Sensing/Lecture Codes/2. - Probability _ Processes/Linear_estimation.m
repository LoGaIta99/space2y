%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% LINEAR ESTIMATION
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear, clc
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MSE = E[ (x^ - x_true)^2 ] = Var(x^) - (E[(x^ - x_true)])^2
% RMSE = sqrt(MSE)
% Ai*A = check bias. The inversion unbiased only if Ai*A = 1
row_names{1} = 'Sample rmse';
row_names{2} = 'Theoretical rmse';
row_names{3} = 'Ai*A';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DATA GENERATION
A = [2;1]; % model
Ne = 1e5; % number of experiments
if 1 % x is a fixed parameter
    x_true = 10*randn*ones(1,Ne); % true value of the parameter to be estimated
else % x is a random variable
    x_true = 10*randn(1,Ne);
end
y_clean = A*x_true; % data without noise
%
% Noise
noise_covariance_model = 3
switch noise_covariance_model
    case 1 % uncorrelated noise with same variance
        Cw = eye(2);
    case 2 % uncorrelated noise with differente variances
        Cw  = diag([100 1]);
    case 3 % correlated noise
        rho = .9999;
        Cw  = [1 rho;rho 1];
    otherwise
end
Cw
% Generation of correlated noise
[U,S] = eig(Cw); % Eigen Value Decomposition: Cw = U*S*U', 
% with U unitary and S diagonal
Vw = U*sqrt(S); % Cw = Vw*Vw'
w = Vw*randn(2,Ne); % noise
if 0 % check that the noise covariance matrix is as expected
    Cw_sample = 1/Ne*w*w'
    return
end
%
% Noisy data
y = y_clean + w;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ESTIMATION
T{1,1} = 'Performance analysis';
T{2,1} = 'sample rmse';
T{3,1} = 'theoretical rmse';
T{4,1} = 'Ai*A';
T{5,1} = 'Ai(1)';
T{6,1} = 'Ai(2)';
for est_technique = 1:7
    switch est_technique
        case 1 % estimation from the first data sample alone
            Ai = [1/A(1) 0];
            T{1,est_technique+1} = 'first sample';
        case 2 % estimation from the second data sample alone
            Ai = [0 1/A(2)];
            T{1,est_technique+1} = 'second sample';
        case 3 % estimation from the difference of the two data samples
            Ai = [1 -1]/(A(1)-A(2));
            T{1,est_technique+1} = 'difference';
        case 4 % LS estimation
            Ai = inv(A'*A)*A';
            T{1,est_technique+1} = 'LS';
        case 5 % BLUE
            Cwi = inv(Cw + 1e-14*eye(2)); % helps inversion of Cw when it's singular
            Ai = inv(A'*Cwi*A)*A'*Cwi;
            T{1,est_technique+1} = 'BLUE';
        case 6 % MMSE (Bayesian)
            s2x = 1; % prior knowledge = variance of x
            Cx = s2x; % 1 x 1 matrix
            Cy = A*Cx*A' + Cw;
            Cyx = A*Cx;
            Ai = Cyx'*inv(Cy);
            T{1,est_technique+1} = 'MMSE s2x = 1';
        case 7 % MMSE (Bayesian)
            s2x = 1000; % prior knowledge = variance of x
            Cx = s2x; % 1 x 1 matrix
            Cy = A*Cx*A' + Cw;
            Cyx = A*Cx;
            Ai = Cyx'*inv(Cy);
            T{1,est_technique+1} = 'MMSE s2x = 1000';
    end
    % Estimation
    x = Ai*y;
    %
    % Evaluation of performance (rmse) assuming knowledge of x_true
    T{2,est_technique+1} = sqrt(mean((x-x_true).^2));
    % Theoretical evaluation under the assumption of unbiasedness
    T{3,est_technique+1}  = sqrt(Ai*Cw*Ai');
    % Check if the inversion is unbiased
    T{4,est_technique+1}  = Ai*A;
    % Estimator
    T{5,est_technique+1}  = Ai(1);
    T{6,est_technique+1}  = Ai(2);
end
% Shows the results
T
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

