clear, clc

t = linspace(-30,30,1001);

f = rectpuls(t);

for n = 1:10
    plot(t,f), grid
    pause
    f = conv2(f,f,'same');
    f = f/max(f);
end
