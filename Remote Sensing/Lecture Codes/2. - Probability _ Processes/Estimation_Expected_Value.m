
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Random_experiments_continuous_variables
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ESTIMATION OF THE EXPECTED VALUE USING THE SAMPLE AVERAGE
if 0 % fine, but slow
    avg = zeros(N,N_repetition);
    for n = 1:N
        avg(n,:) = mean(x(1:n,:),1);
    end
else % fine as well, but fast
    cumulative_sum = cumsum(x,1);
    avg = cumulative_sum./( (1:N)'*ones(1,N_repetition) );
end
figure
subplot(2,1,1)
plot(1:N,avg), grid % observe the convergence with N
xlabel('Number of samples'), title('Estimation of E[x]')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% EVALUATION OF THE DISPERSION OF THE SAMPLE AVERAGE
% SAMPLE AVERAGE = ESTIMATOR OF THE EXPECTED VALUE
Variance_avg = mean(avg.^2,2) - (mean(avg,2)).^2;
Disp_avg = sqrt(Variance_avg);
Disp_avg_th = sqrt(Vx_th./(1:N));
subplot(2,1,2)
if 0
    plot(1:N,Disp_avg,1:N,Disp_avg_th), grid %
    xlabel('Number of samples')
    title('Dispersion')
else
    semilogy(1:N,Disp_avg,1:N,Disp_avg_th), grid %
    xlabel('Number of samples'), title('Dispersion')
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%