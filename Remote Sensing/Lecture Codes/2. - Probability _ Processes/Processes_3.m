%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if 0 % load an audio track
    clear, clc
    track = 1
    switch track
        case 1
            [x,Fs] = audioread('01 Track 1.mp3');
            t_min = 5;
        otherwise
    end
    Fs % Sampling Frequency [Hz]
    % cut the time axis
    t_max = t_min + 20
    ind = round(t_min*Fs):round(t_max*Fs);
    x = x(ind,:);
    dt = 1/Fs;
    Nt = length(ind);
    Tobs = Nt*dt
    t = (0:Nt-1)*dt + t_min;
    return
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Power Spectral Densities of the left and right tracks
Nf = 2^ceil(log2(Nt))
% FT
[X,f] = dft(x,t,Nf);
X = X*dt; % scale factor for dimensional consistentcy 
% Power Spectral Densities
Sx = 1/Tobs*abs(X).^2;
figure
subplot(2,1,1), plot(f,Sx), grid
xlabel('frequency [Hz]'), title('S_x(f)')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Autocorrelation by inverse FT
n_max = 100;
delta_t = (-n_max:n_max)*dt;
Rx = idft(Sx,f,delta_t); 
% check: Rx should be real since x is a real-valued process
max_err_imag = max(abs(imag(Rx(:))))
Rx = real(Rx); 
% scale factor for dimensional consistentcy 
% (!!!: idft is based on built-in function ifft, that divides the result by
% the number of frequency points
df = f(2)-f(1);
Rx = Rx*df*Nf; 
subplot(2,1,2), plot(delta_t,Rx), grid
xlabel('time lag [s]'), title('R_x(t)')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Cross-correlation between the left and right audio tracks
R_lr = zeros(2*n_max+1,1);
tic
for n = -n_max:n_max
    xxx = x(:,1).*circshift(x(:,2),[n 0]); % circshift = circular shift
    R_lr(n+n_max+1,:) = mean(xxx,1);
end
time_comp = toc
figure
subplot(2,1,1), plot(delta_t,R_lr), grid
xlabel('time lag [s]')
title(['R_l_r(t) - computational time = ' num2str(time_comp) ' s'])
%

% Same as above but using FT
tic
S_lr = 1/Tobs*X(:,1).*conj(X(:,2)); % cross-spectrum 
R_lr = idft(S_lr,f,delta_t); 
time_comp = toc
% check: Rx should be real since x is a real-valued process, but a very
% small imaginary part will be present due to numerical errors
max_err_imag = max(abs(imag(R_lr(:))))
R_lr = real(R_lr); 
% scale factor for dimensional consistentcy 
% (!!!: idft is based on built-in function ifft, that divides the result by
% the number of frequency points
df = f(2)-f(1);
R_lr = R_lr*df*Nf;
subplot(2,1,2), plot(delta_t,R_lr), grid
xlabel('time lag [s]')
title(['R_l_r(t) - computational time = ' num2str(time_comp) ' s'])
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

