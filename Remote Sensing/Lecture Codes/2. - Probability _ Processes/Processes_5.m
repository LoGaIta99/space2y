%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% IMAGE ANALYSIS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear, clc

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% loads an RGB image
z = imread('Photo0075.jpg');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% conversion to double - required to manipulate data
I = double(z);
% scaling - Class double RGB images need to be scaled between 0 and 1
I = I/max(I(:));
%figure, imagesc(I)
% Exctraction of a single channel
I = I(:,:,1);
[Ny,Nx] = size(I);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DEGRADATION AND RESTORATION
figure
for step = 1:3
    if step == 2 % Injection of white noise
        sw = 1; % noise std (= sqrt of noise power)
        I = I + sw*randn(Ny,Nx);
    end
    if step == 3 % restoration through low-pass filtering
        % Low pass filter
        % fractional bandwidth along columns and rows
        By = .15;
        Bx = .15;
        filter_y = fir1(30,By);
        filter_x = fir1(30,Bx);
        t = conv2(I,filter_x(:)','same'); % row filter
        I = conv2(t,filter_y(:),'same'); % column filter
    end
    
    subplot(2,3,step), imagesc(I), colormap('gray')
    xlabel('x'), ylabel('y'), title('Image')
    
    
    % SPECTRAL ANALYSIS
    Nfy = 2^ceil(log2(Ny));
    Nfx = 2^ceil(log2(Nx));
    fy = (-Nfy/2:Nfy/2)/Nfy; % normalized frequency along columns
    fx = (-Nfx/2:Nfx/2)/Nfx; % normalized frequency along rows
    t = fft(I,Nfy,1); % Fourier transform along columns
    t = fft(t,Nfx,2); % Fourier transform along rows
    If = fftshift(t);
    % Spectrum
    S = 1/Nx/Ny*abs(If).^2;
    subplot(2,3,step+3),
    imagesc(fx,fy,10*log10(abs(S))), colorbar, axis xy
    xlabel('f_x'), ylabel('f_y'), title('Spectrum [dB]')
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

