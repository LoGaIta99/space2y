%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ESTIMATION OF THE PROBABILITY P(x0 <= x < x0 + dx) 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Random_experiments_continuous_variables
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% STIMA
Nbin = length(x0); % number of intervals
P = zeros(Nbin,N_repetition);
tic
for e = 1:N_repetition
    if 0 % building up the histograms
        for k = 1:Nbin
            n = sum(and(x0(k)<=x(:,e),x(:,e)<x0(k)+dx));
            P(k,e) = n/N;
        end
    else % Matlab built-in function (much faster)
        h = hist(x(:,e),x0);
        P(:,e) = h/N;
    end
end
toc


sum(P,1) % check - must yield 1
figure,
subplot(2,1,1), plot(x0,P), grid, xlabel('x'), title('P(x0<=x<x0+dx)')
% Estimation of the probability density function (normalization by dx)
fx = P/dx;
subplot(2,1,2), plot(x0,fx), grid, xlabel('x'), title('f_x(x)')
% Theoretical PDF
hold on, plot(x0,fx_th,'k')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ERROR ANALYSIS
% For each bin, P is a random variable for which we have 
% N_repetition realizations
EP = mean(P,2); % estimation of the expected value of P
VP = mean(P.^2,2) - EP.^2; % estimation of the variance of P
SP = sqrt(VP); % estimation of the dispersion of P

figure
p = fx_th*dx; % probability for each bin
EP_th = p;
SP_th = sqrt(p.*(1-p)/N);
subplot(2,1,1), plot(x0,EP,x0,EP_th), grid
xlabel('x'), title('E[P]')
subplot(2,1,2), plot(x0,SP,x0,SP_th), grid
xlabel('x'), title('std[P]')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

