%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ESTIMATION OF THE JOINT PROBABILITY OF TWO CONTINUOUS RANDOM VARIABLES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
N = 1e4 % number of experiments
% Generation of two uniform random variables (-.5,.5)
u = rand(N,1)-0.5;
v = rand(N,1)-0.5;

% Change of variables
theta = pi/6;
x = cos(theta)*u + sin(theta)*v;
y = -sin(theta)*u + cos(theta)*v;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% intervals for x and y
dx = 1e-1;
dy = 1e-1;
x_min = min(x)-.5; x_max = max(x)+.5;
y_min = min(y)-.5; y_max = max(y)+.5;
bin_y = (y_min:dy:y_max);
bin_x = (x_min:dx:x_max);
% 2D histogram using Matlab built-in function hist3 - see help hist3
bin{1} = bin_y;
bin{2} = bin_x;
h = hist3([y x],bin);
whos bin
% Estimated joint pdf
fxy = h/N/dx/dy;
sum(fxy(:))*dx*dy % check - must yield 1
figure
imagesc(bin_x,bin_y,h), axis xy
title('f(x,y)'), ylabel('y'), xlabel('x')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


