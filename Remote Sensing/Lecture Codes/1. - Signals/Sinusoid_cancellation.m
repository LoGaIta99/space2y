clear, clc

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Signal
fs = 8192;
dt = 1/fs;
t = 0:dt:1-dt;
Nt = length(t);
% Frequency samples
Nf = 2^ceil(log2(Nt));
Nf = 4*Nf; % oversampling in the frequency domain
%
f = (-Nf/2:Nf/2-1)/Nf/dt;
k0 = find(f==440);
case_numb = 2
switch case_numb
    case 1 % the sinusoid frequency is within the samples of the FT
        f0 = f(k0)
    case 2 % the sinusoid frequency is NOT within the samples of the FT
        f0 = f(k0) + pi/10
    otherwise
end
s = exp(1i*2*pi*f0*t');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Fourier transform
S = fftshift(fft(s,Nf));
figure, subplot(4,1,1), plot(f,abs(S)), grid
xlim(f0+[-10 10]), xlabel('frequency [Hz]')
title('Signal')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Peak estiamtion
[Sm,k] = max(S);
A_stim = Sm/Nt;
amp_err = abs(A_stim) - 1 % true amplitude = 1
phase_err = angle(A_stim) % true phase = 0
f0_estim = f(k);
err_freq = f0_estim-f0 % true frequency = f0
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Cancellation by coherent subtraction
ddd = (A_stim*exp(1i*2*pi*f0_estim*t(:))); 
s_clean = s - ddd;
S_clean = fftshift(fft(s_clean,Nf));
subplot(4,1,2), plot(f,abs(S_clean)), grid
xlim(f0+[-10 10]), xlabel('frequency [Hz]')
title('Subtraction')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Cancellation by zeroing the peak
S_clean = S;
S_clean(k) = 0;
subplot(4,1,3), plot(f,abs(S_clean)), grid
xlim(f0+[-10 10]), xlabel('frequency [Hz]')
title('Peak-zeroing')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Cancellation by filtering
df = f(2)-f(1);
B = 50*df;
Bn = B*dt;
Nh = 2*ceil(2/2/Bn);
th = (-Nh/2:Nh/2)*dt;
h_low_pass = fir1(Nh,Bn); % see the fir1 page on Matlab documentation
h_band_pass = h_low_pass.*exp(1i*2*pi*f0_estim*th);
delta = double(th==0);
h_band_stop = delta - h_band_pass;

% Analysis and application 
H = fftshift(fft(h_band_stop,Nf));
figure, 
subplot(3,1,1), plot(f,abs(H)), grid
xlabel('frequency [Hz]'), title('FT of the filter')
subplot(3,1,2), plot(f,abs(H(:).*S)), grid
xlim(f0+[-10 10]), xlabel('frequency [Hz]')
title('FT(filter) * FT(signal)')
s_clean = conv2(s(:),h_band_stop(:));
subplot(3,1,3), plot(abs(s_clean)), grid
xlabel('samples'), title('convolution')

S_clean = fftshift(fft(s_clean,Nf));
figure(1)
subplot(4,1,4), plot(f,abs(S_clean)), grid
xlim(f0+[-10 10]), xlabel('frequency [Hz]')
title('Filtering')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

