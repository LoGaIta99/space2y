%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SIGNAL REPRESENTATION
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear % Clear memory
clc % Clear command window
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% time axis
dt = 0.1 % sampling time
t = (-250:250)*dt; % time axis (vector)
N = length(t) % number of time samples
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Examples of signals
Tx = 2;
tau_x = 1;
x = sinc((t-tau_x)/Tx);
figure
subplot(4,1,1), plot(t,x), grid, ylim([-1.2 1.2])
xlabel('time [s]'), title('x(t)')

Ty = 3;
tau_y = -1;
y = rectpuls((t-tau_y)/Ty);
subplot(4,1,2), plot(t,y), grid, ylim([-1.2 1.2])
xlabel('time [s]'), title('y(t)')

s = x + y; % sum
subplot(4,1,3), plot(t,s), grid
xlabel('time [s]'), title('sum')

z = x.*y; % product
subplot(4,1,4), plot(t,z), grid
xlabel('time [s]'), title('product')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%













