clear, clc

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Time axis
% sampling time
dt = 1e-2 % s  
% observation time
t_obs = 2; % s 
t = (-t_obs/2:dt:t_obs/2); 
Nt = length(t)

% time axis of the filter
th = (-50:50)*dt; %
Bh = .1/dt; % filter bandwidth
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% filter
h = Bh*sinc(th*Bh);
% Signal
for signal = 5%1:5
    switch signal
        case 1 % Bx<Bh
            Bx = Bh/2;
            x = sinc(t*Bx);
        case 2 % Bx>Bh
            Bx = Bh*2;
            x = sinc(t*Bx);
        case 3 % f0<Bh
            f0 = Bh/3;
            x = cos(2*pi*f0*t);
        case 4 % f0>Bh
            f0 = Bh*2;
            x = cos(2*pi*f0*t);
        case 5
            Bx = Bh;
            x = sinc((t-.5)*Bx);
        otherwise
    end
    
    y = conv2(x,h,'same')*dt;
    
    figure
    subplot(3,1,1), plot(t,x), grid, xlabel('time [s]'), ylabel('x(t)')
    subplot(3,1,2), plot(th,h), grid, xlabel('time [s]'), ylabel('h(t)')
    xlim([t([1 end])])
    subplot(3,1,3), plot(t,y), grid, xlabel('time [s]'), ylabel('y(t)')
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%