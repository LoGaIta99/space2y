%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PROBABILITY OF FALSE ALARM AND CORRECT DECISION
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all
close all
clc

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Generation of complex noise
N = 1e5; % number of realizations
s2w = .1; % power (variance)
w = sqrt(s2w/2)*(randn(N,1)+1i*randn(N,1));
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% NOISE STATISTICS
s = w;
% Absolute value
s_abs = abs(s);
% Sample and theoretical probability density function (Rayleigh distribution)
[h,a] = hist(s_abs,100);
da = a(2)-a(1);
p_sample = h/sum(h)/da;
p_th = 2*a/s2w.*exp(-a.^2/s2w);
figure, plot(a,p_sample,a,p_th), grid
xlabel('a'), title('Rayleigh distribution')

% Pfa = Probability that abs(noise)exceeds Vt
Vt = a; % Threshold
Pfa_th = exp(-Vt.^2/s2w);
for v = 1:length(Vt)
    Pfa(v) = sum(s_abs>Vt(v))/N;
end
figure, plot(Vt,Pfa,Vt,Pfa_th), grid
xlabel('Vt'), title('P_f_a')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PROBABILITY OF CORRECT DECISION
% Pd = Probability that abs(signal+noise)exceeds Vt
figure,
for SNR = [0 .1 .5 1 5 10];
    A = sqrt(SNR*s2w); % Signal peak amplitude
    s = A + w;
    s_abs = abs(s);
    for v = 1:length(Vt)
        Pd(v) = sum(s_abs>Vt(v))/N;
    end
    plot(Pfa,Pd), grid on, hold on
end
axis([0 1 0 1])
xlabel('P_f_a'), ylabel('P_d')
title('ROC Curves')
% ROC = Receiver Operating Characteristics 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%