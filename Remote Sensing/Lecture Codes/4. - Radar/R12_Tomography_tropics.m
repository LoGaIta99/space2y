%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% EXAMPLE OF APPLICATION OF TOMOGRAPHIC SAR PROCESSING FOR 3D IMAGING OF A
% TROPICAL FOREST
% THIS SCRIPT MAKES USE OF A SUBSET OF THE SAR DATA COLLECTED BY ONERA
% DURING THE CAMPAIGN TROPISAR, FOUNDED BY THE EUROPEAN SPACE AGENCY IN THE
% THE CONTEXT OF PHASE-0 STUDY FOR THE P-BAND MISSION BIOMASS
% THE FINAL REPORT OF THE TROPISAR CAMPAIGN CAN BE DOWNLOADED AT
% https://earth.esa.int/c/document_library/get_file?folderId=21020&name=DLFE-899.pdf
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DATA LOADING
clear all
close all
clc

load('SAR_Tomography_tropics.mat')
whos
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[Nr,Nx,N] = size(I_SAR)

% let's take a look
for n = 1:6
    subplot(2,3,n), 
    temp = abs(I_SAR(:,:,n));
    imagesc(20*log10(temp))
end

% trajectories
figure
subplot(2,1,1), plot(x_ax,Sy), grid
subplot(2,1,2), plot(x_ax,Sz), grid

plot3(x_ax,Sy,Sz), grid, 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% topography
Z = zeros(Nr,Nx); % flat topography
R = r_ax(:)*ones(1,Nx); % range for the Master (reference) image

% R = sqrtr( (Sz-Z)^2+ (Sy-Y).^2 ) invert this eq to get Y
Y = ones(Nr,1)*Sy(Master,:) + sqrt(R.^2 - (ones(Nr,1)*Sz(Master,:)-Z).^2);

% reference geometry for each flight
R_ref = zeros(Nr,Nx,N);
Theta = zeros(Nr,Nx,N);
for n = 1:N
    delta_y = Y - ones(Nr,1)*Sy(n,:);
    delta_z = Z - ones(Nr,1)*Sz(n,:);
    R_ref(:,:,n) = sqrt(delta_y.^2 + delta_z.^2);
    Theta(:,:,n) = acos(delta_z./R_ref(:,:,n));
end
% I can compensate for the distances
I_comp = I_SAR.*exp(1i*4*pi/lambda*R_ref);

% phase to height conv factors
kz = zeros(Nr,Nx,N);
for n = 1:N
    delta_teta = Theta(:,:,n) - Theta(:,:,Master);
    kz(:,:,n) = 4*pi/lambda*delta_teta./sin(Theta(:,:,Master));
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Tomography
dz = 3;
z_ax = (-70:dz:50); Nz = length(z_ax);
Tomo = zeros(Nr,Nx,Nz);
for z = 1:Nz
    fase = kz*z_ax(z);
    ttt = I_comp.*exp(-1i*fase);
    Tomo(:,:,z) = mean(ttt,3);
end

% despeckling
Tomo_intens = zeros(Nr,Nx,Nz);
for z = 1:Nz
    ttt = real(Tomo(:,:,z).*conj(Tomo(:,:,z)));
    ttt = conv2(ttt,hamming(11),'same');
    ttt = conv2(ttt,hamming(11)','same');
    Tomo_intens(:,:,z) = ttt;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TOMO MOVIE

A = log(mean(abs(I_comp),3));

figure, 
for r = 1:3:Nr
    ttt = squeeze(Tomo_intens(r,:,:))';
    ttt = ttt./(ones(Nz,1)*mean(ttt,1));
    
    subplot(2,1,1), imagesc(x_ax,r_ax,A), 
    hold on, plot(x_ax([1 end]),r_ax(r)*[1 1],'r'), hold off
    axis equal tight
    xlabel('azimuth [m]'), ylabel('range [m]'), title('SAR Imaging')
    subplot(2,1,2)
    imagesc(x_ax,z_ax,ttt), axis xy, 
    xlabel('azimuth [m]'), ylabel('height [m]'), title('SAR Tomography')

    drawnow
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%














