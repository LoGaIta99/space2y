clear, clc
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% EVALUATION OF RECEIVED POWER FROM DISTRIBUTED TARGETS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PARAMETERS
f0 = 1e9; % central frequency
c = 3e8; % speed of light
s0 = 10^-(1.5); % backscatter coefficient (soil & rocks)
H = 4000; % flight height 
dphi = 40/180*pi; % azimuth beamwidth
dteta = 40/180*pi; % elevation beamwidth
teta0 = 40/180*pi; % antenna pointing
B = 100e6; % transmitted bandwdith
% transmitted power
Pt = 1 % 1 W peak power (1 W = 30 dBm)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DERIVATION OF RECEIVED POWER
lambda = c/f0; % wavelength
pho_r = c/2/B; % range resolution
r_min = H/cos(teta0-dteta/2); % min range
r_max = H/cos(teta0+dteta/2); % max range

r = linspace(r_min,r_max,100); % range axis
teta = asin(sqrt(1 - (H./r).^2)); % incidence angle

L = lambda/(40/180*pi); % antenna length
A = L*L; % antenna area

% RADAR EQUATION (step by step)
G = 4*pi/(lambda^2)*A % antenna gain
area_target = pho_r.*dphi.*r; % illuminated area within one resolution cell
RCS = area_target*s0; % RCS of all targets within one res cell
f_dir = sinc((teta-teta0)/dteta).^2;
% Received power
Pr = Pt./( (4*pi*r.^2).^2 )*G*A.*(f_dir.^2).*RCS;

figure; 
subplot(2,1,1), plot(r,10*log10(Pr)), grid
xlabel('range [m]'), title('Received power [dB]') 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% GENERATION OF A SIGNAL FROM DISTRIBUTED TARGETS
% Targets per resolution cell
N_target_per_resolution_cell = 20
% spacing in slant range
dr = pho_r/N_target_per_resolution_cell; 
% range axis
r_ax = r_min:dr:r_max;
Nr = length(r_ax);
% Total received power in each resolution cell
Pr = interp1(r,Pr,r_ax);
% Power from a single target
P_target = Pr/N_target_per_resolution_cell;
% Phase = random variable 
phi = 2*pi*(rand(1,Nr)-.5);
a = sqrt(P_target).*exp(1i*phi);

% Ideal transmitted pulse
rg = (-300:300)*dr;
g = sinc(rg/pho_r);

% Received signal = sum of signals from all targets = convolution
s_rx = conv2(a,g,'same');

% One-sample power estimation
Pr_stim = real(s_rx.*conj(s_rx));
subplot(2,1,2), plot(r_ax,10*log10(Pr_stim)), grid
xlabel('range [m]'), title('Received power [dB]') 

% Power estimation by temporal average
Nt = 100;
avg_filter = ones(1,Nt)/Nt;
Pr_stim_avg = conv2(real(s_rx.*conj(s_rx)),avg_filter,'same');
subplot(2,1,1), hold on, plot(r_ax,10*log10(Pr_stim_avg)), 
subplot(2,1,2), hold on, plot(r_ax,10*log10(Pr_stim_avg)), 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% EVALUATION OF ACCURACY BY ANALYSIS OF MULTIPLE REALIZATIONS
N_realizations = 100;
s_rx = zeros(N_realizations,Nr);
for n = 1:N_realizations
    phi = 2*pi*(rand(1,Nr)-.5);
    an = sqrt(P_target).*exp(1i*phi);
    s_rx(n,:) = conv2(an,g,'same');
end
Pr_stim_avg = conv2(real(s_rx.*conj(s_rx)),avg_filter,'same');
EP = mean(Pr_stim_avg,1);
VP = mean(Pr_stim_avg.^2,1) - EP.^2; % VP = EP^2/N_eq
N_eq = (Pr.^2)./VP;
figure, plot(r_ax,N_eq), grid
title('Estimated Number of Equivalent Indipendent Samples')
xlabel('range [m]'),
N_eq_avg = mean(EP.^2)/mean(VP)
T_avg = Nt*(dr*2/c); % time duration of Nt range samples
N_eq_th = T_avg*B
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%