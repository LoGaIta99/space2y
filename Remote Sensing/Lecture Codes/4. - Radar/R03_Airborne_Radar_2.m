clear, clc

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% EVALUATION OF SNR AFTER MATCHED FILTERING
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PARAMETERS
f0 = 1e9; % central frequency
c = 3e8; % speed of light
s0 = 10^-(1.5); % backscatter coefficient (soil & rocks)
H = 4000; % flight height 
dphi = 40/180*pi; % azimuth beamwidth
dtheta = 40/180*pi; % elevation beamwidth
theta0 = 40/180*pi; % antenna pointing
B = 100e6; % transmitted bandwdith
% transmitted power
Pt = 1 % 1 W peak power (1 W = 30 dBm)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DERIVATION OF RECEIVED POWER
lambda = c/f0; % wavelength
rho_r = c/2/B; % range resolution
r_min = H/cos(theta0-dtheta/2); % min range
r_max = H/cos(theta0+dtheta/2); % max range

r = linspace(r_min,r_max,100); % range axis
teta = asin(sqrt(1 - (H./r).^2)); % incidence angle

L = lambda/(40/180*pi); % antenna length
A = L*L; % antenna area

% RADAR EQUATION (step by step)
G = 4*pi/(lambda^2)*A % antenna gain
area_target = rho_r.*dphi.*r; % illuminated area within one resolution cell
RCS = area_target*s0; % RCS of all targets within one res cell
f_dir = sinc((teta-theta0)/dtheta).^2;
% Received power
Pr = Pt./( (4*pi*r.^2).^2 )*G*A.*(f_dir.^2).*RCS;

figure; 
subplot(2,1,1), plot(r,10*log10(Pr)), grid
xlabel('range [m]'), title('Received power [dB]') 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% NOISE PARAMETERS
K = 1.38e-23; % Boltzmann constant
F = 4; % noise figure
N0 = K*290*F; % Power spectral density of noise
Pnoise = N0*B; % noise power before filtering
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SIGNAL TO NOISE RATIO FOR TWO DIFFERENT CONFIGURATIONS WITH THE SAME
% RANGE RESOLUTION AND TRANSMITTED POWER
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TRANSMISSION OF A SHORT PULSE AND NO FILTERING
figure
subplot(2,1,1), plot(r,10*log10(Pr/Pnoise)), grid
xlabel('range [m]'), title('SNR - transmission of a short pulse and no filtering [dB]') 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TRANSMISSION OF A LONG PULSE AND MATCHED FILTERING
PRI = 2*r_max*2/c; % a factor of 2 is assumed to leave some margin
duty_cycle = 0.8; % typical value for an FMCW system
Tobs = PRI*duty_cycle; % observation time
Er = Pr*Tobs; % Received energy
SNR = Er/N0; % SNR

subplot(2,1,2), plot(r,10*log10(SNR)), grid
xlabel('range [m]'), title('SNR - transmission of a long pulse and matched filtering [dB]') 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
