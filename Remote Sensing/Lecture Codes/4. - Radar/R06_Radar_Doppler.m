clear all
close all
clc

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% RADAR PARAMETERS
c = 3e8; % propagation velocity
f0 = 30e9; % carrier frequency
lambda = c/f0;
B = 10e6; % transmitted bandwidth
Tobs = 30e-6; % signal duration
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TARGET RANGE AND RADIAL VELOCITY
r0 = 4500; % m
tau = 2/c*r0; % delay
v0 = 100; % m/s
fd = 2/c*f0*v0; % Doppler frequency
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TRANSMITTED AND RECEIVED WAVEFORMS
dt = 1/4/B; % sampling interval (up-sampled by a factor 4)
Nt = 4*round(Tobs/dt) + 1; % Nt is odd to get symmetry
t = (-(Nt-1)/2:(Nt-1)/2)*dt; % time axis

waveform = 2
% transmitted signal
[g,signal] = pulse_generator(t,Tobs,B,waveform);
% received signal
g_rx = interp1(t,g,t-tau,'linear',0); % delayed waveform
fase = 2*pi*f0*tau + 2*pi*fd*t; % phase
s_rx = g_rx.*exp(-1i*fase);
% Fourier Transform of the transmitted waveform
Nf = 2*Nt;
f = (-Nf/2:Nf/2-1)/Nf/dt;
G = fftshift(fft(g,Nf));
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SIMULAZIONE DI RICEZIONE CON CANALI PER LA STIMA DI RANGE E DOPPLER

% Set of Doppler frequencies 
df = 0.25/Tobs;
f_max = min(0.5/dt,30*df);
f_dop_ax = (-f_max:df:f_max);
M = length(f_dop_ax) 

for m = 1:length(f_dop_ax);
    % frequency shift
    ss = s_rx.*exp(1i*2*pi*f_dop_ax(m)*t);
    % range compression
    s_rc_dop(m,:) = conv2(ss,conj(fliplr(g)),'same') * dt;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% FIGURES
% range and velocity axes
r = c/2*t;
v = c/2*f_dop_ax/f0;
% range and velocity resolutions
rho_r = c/2/B
rho_v = c/2/f0/Tobs

figure
subplot(2,1,1), plot(t,abs(g)), grid, ylim([0 max(abs(g_rx))*1.1])
xlabel('time [s]'), title(['waveform = ' signal])
subplot(2,1,2), plot(f,abs(G)), grid, ylim([0 max(abs(G))*1.1])
xlabel('frequency [Hz]'), title(['waveform = ' signal])

figure
surf(r,v,abs(s_rc_dop)), 
shading interp, colormap('jet')
title(['Ambiguity function - waveform = ' signal])
xlabel('range [m]'), ylabel('velocity [m/s]'),
xlim([0 r(end)])

figure
imagesc(r,v,abs(s_rc_dop)), axis xy, colormap('jet')
xlim([r0-5*rho_r r0+5*rho_r])
ylim([v0-5*rho_v v0+5*rho_v])
xlabel('range [m]')
ylabel('velocity [m/s]')
hold on, plot(r0,v0,'ko')
title('Ambiguity function - close up')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
