clear all
close all
clc

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%% MULTI-PASS SAR INTERFEROMETRY %%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%% DATA ACQUISITION %%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% RADAR PARAMETERS
% wavelength
c = 3e8;
f0 = 5e9;
lambda = c/f0;
% range resolution
rho_r = 5;
B = c/2/rho_r; % bandwidth
% azimuth resolution
rho_x = 5;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% GEOMETRY
H = 600e3;  % orbital altitude
theta_mid = 30/180*pi; % pointing in elevation
y_med = H*tan(theta_mid)
N = 10 % number of orbits
Z = H*ones(1,N);
Y = (0:N-1)*200;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SCENE 
% target coordinates. y = ground range. x = azimuth
dy = rho_r/3;
dx = rho_x/3;
y_ax = [-500:dy:500] + y_med;
Ny = length(y_ax);
x_ax = [-500:dx:500];
Nx = length(x_ax);
[y,x] = ndgrid(y_ax,x_ax);
% topography
z_max = 25; 
z = z_max*exp(-((y-mean(y(:)))/300).^2).*exp(-((x-mean(x(:)))/300).^2);
% distances 
for n = 1:N
    R(:,:,n) = sqrt((y-Y(n)).^2 + (z-Z(n)).^2);
end
% interpolation from ground range to slant range
dr = rho_r/4;
r_min = min(min(R(:,:,1)));
r_max = max(max(R(:,:,1)));
r_ax = [r_min:dr:r_max]';
Nr = length(r_ax);
for x = 1:Nx
    r = R(:,x,1);
    zr(:,x) = interp1(r,z(:,x),r_ax);
end
zr(isnan(zr)) = 0;
[r,x] = ndgrid(r_ax,x_ax);
% ground range as a function of slant range y = y(r)
yr = sqrt(r.^2 - (zr-Z(1)).^2 ) + Y(1);

figure, imagesc(x_ax,y_ax,z,[0 z_max*1.2]), colorbar, colormap('jet')
title('topography z(x,y)')
figure, imagesc(x_ax,r_ax,zr,[0 z_max*1.2]), colorbar, colormap('jet')
title('topography z(x,r)')

% Distances in each pass as function of the distance in the first pass
clear R
for n = 1:N
    R(:,:,n) = sqrt((yr-Y(n)).^2 + (zr-Z(n)).^2);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TARGET REFELCTIVITY 
targets = randn(Nr,Nx) + 1i*randn(Nr,Nx);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SAR IMAGES 
% range filter
t = [-20:20]*dr; fr = exp(-(t/rho_r).^2);
% azimuth filter
t = [-20:20]*dx; fx = exp(-(t/rho_x).^2);
Dc = zeros(Nr,Nx,N);
for n = 1:N
    t = targets.*exp(1i*4*pi/lambda*R(:,:,n));
    t = conv2(t,fr(:),'same');
    Dc(:,:,n) =  conv2(t,fx(:)','same');
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if 0 % take a look at the data
    for n = [1 2 3 4 5 6 10]
        figure, imagesc(abs(Dc(:,:,n)))
        title(num2str(n))
    end
    return
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%% DATA PROCESSING %%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% REFERENCE DISTANCES
% reference topography
zr_ref = zeros(Nr,Nx);
% evaluation of y = y(r)
yr_ref = sqrt(r.^2 - (zr_ref-Z(1)).^2 ) + Y(1);
% distances in each pass as functions of the distance in the first pass
for n = 1:N
    R_ref(:,:,n) = sqrt((yr_ref-Y(n)).^2 + (zr_ref-Z(n)).^2);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% HEIGHT TO PHASE CONVERSION FACTORS
teta = atan((Y-y_med)./Z);
delta_teta = teta(1)-teta;
Kz = 4*pi/lambda*delta_teta/sin(teta(1));
z_amb = 2*pi./Kz
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ANALYSIS OF THE INSAR PHASE
% interferometric pairs
n = 1
m = 3
I = Dc(:,:,n).*conj(Dc(:,:,m));
phi_ref = 4*pi/lambda*(R_ref(:,:,n)-R_ref(:,:,m));
Ic = Dc(:,:,n).*conj(Dc(:,:,m)).*exp(-1i*phi_ref);
% filters (de-speckle)
filtro_rg = hamming(21);
filtro_az = hamming(21);
En = conv2(conv2(Dc(:,:,n).*conj(Dc(:,:,n)),filtro_rg(:),'same'),filtro_az(:)','same');
Em = conv2(conv2(Dc(:,:,m).*conj(Dc(:,:,m)),filtro_rg(:),'same'),filtro_az(:)','same');
If = conv2(conv2(Ic,filtro_rg(:),'same'),filtro_az(:)','same');
coe = If./sqrt(En.*Em);

figure,  imagesc(x_ax,r_ax,abs(Dc(:,:,n)))
xlabel('azimuth [m]'), ylabel('slant range [m]'), title(['Image ' num2str(n)])
colormap('jet')
figure, imagesc(x_ax,r_ax,abs(Dc(:,:,m)))
xlabel('azimuth [m]'), ylabel('slant range [m]'), title(['Image ' num2str(m)])
colormap('jet')

figure
subplot(2,3,1), imagesc(x_ax,r_ax,angle(I),[-pi pi]), colorbar
xlabel('azimuth [m]'), ylabel('slant range [m]'), title('InSAR phase')
subplot(2,3,2), imagesc(x_ax,r_ax,angle(Ic),[-pi pi]), colorbar
xlabel('azimuth [m]'), ylabel('slant range [m]'), title('Compensated InSAR phase')
subplot(2,3,3), imagesc(x_ax,r_ax,angle(If),[-pi pi]), colorbar
xlabel('azimuth [m]'), ylabel('slant range [m]'), title('Filtered InSAR phase')
colormap('jet')

% Conversion from phase to height
Kz_nm = Kz(m)-Kz(n);
zr_stim = zr_ref + angle(If)/Kz_nm;
subplot(2,3,4), imagesc(x_ax,r_ax,abs(coe), [0 1]), colorbar
xlabel('azimuth [m]'), ylabel('slant range [m]'), title('Coherence')
subplot(2,3,5), imagesc(x_ax,r_ax,zr_stim), colorbar
xlabel('azimuth [m]'), ylabel('slant range [m]'), title('estimated topography')
colormap('jet')

% Phase Unwrapping
phiu = unwrap(angle(If),[],1);
zr_stim = zr_ref + phiu/Kz_nm;
subplot(2,3,6), imagesc(x_ax,r_ax,zr_stim), colorbar
xlabel('azimuth [m]'), ylabel('slant range [m]'), title('Estimated topo after Phase Unwrapping')
colormap('jet')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
