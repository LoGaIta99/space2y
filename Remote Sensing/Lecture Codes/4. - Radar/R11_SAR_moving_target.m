clear all
close all
clc

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ACQUISITION AND FOCUSING OF SAR DATA IN THE PRESENCE OF A MOVING TARGET
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% RADAR PARAMETERS
c = 3e8; % wave propagation velocity [m/s]
f0 = 1e10; % carrier frequency [Hz]
lambda = c/f0 % wavelength [m]
pho_r = 10 % range resolution [m]
B = c/2/pho_r; % bandwidth [Hz]
pho_x = 10 % along-track resolution (azimuth) [m]
Lx = 2*pho_x % physical antenna length (azimuth
delta_psi = lambda/Lx; % illumination angle
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% FLIGHT GEOMETRY
H = 650e3; % orbit height [m]
v = 7000; % velocity w.r.t. Earth [m/s]
teta0 = 30/180*pi; % pointing in elevation [rad]
y_med = H*tan(teta0); % ground range min and max
y_min = y_med - 1000;
y_max = y_med + 1000;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SYNTHETIC APERTURE
r_min = sqrt(y_min^2 + H^2); % slant range min
r_max = sqrt(y_max^2 + H^2); % slant range max
As = lambda/Lx*r_max % maximum synthetic aperture
dx_ant = Lx/2*0.5; % spatial sampling of the synthetic aperture
x_amb = lambda/2/dx_ant*r_min % ambiguous interval if no antenna pattern is used
PRI = dx_ant/v; % Pulse Repetition Interval
PRF = 1/PRI; % Pulse Repetition Frequency
Ltot = 4*As; % Total length of the simulated orbit
xa = (-Ltot/2:dx_ant:Ltot/2); % Radar position along the orbit
Nx = length(xa);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Time and range axis
delay_min = r_min*2/c; %
delay_max = r_max*2/c; %
% time axis
dt = 1/2/B; % sampling interval
t = (delay_min:dt:delay_max);
r = t*c/2;  Nr = length(r); % distances
dr = r(2)-r(1);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TARGET POSITION AND VELOCITY
x0 = 0;
r0 = mean(r);
vr = 1; % m/s
vx = 0; % m/s
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% RAW DATA AFTER MATCHED FILTERING
Drc = zeros(Nx,Nr);
tau = xa/v;
for n = 1:Nx % loop on Radar position
    rn = r0 + vr*tau(n);
    xn = x0 + vx*tau(n);
    Rn = sqrt( (xa(n)-xn).^2 + rn^2);
    psi_n = (xa(n)-xn)./Rn;
    f = sinc(psi_n/delta_psi).^2; % antenna pattern
    g = f*sinc((r-Rn)/pho_r)*exp(-1i*4*pi/lambda*Rn);
    Drc(n,:) = g;
end
figure,
imagesc(r,xa,abs(Drc)), axis xy , title('range compressed data - abs')
xlabel('slant range [m]'), ylabel('sensor position [m]')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% FOCUSING BY 1D MATCHED FILTERING
Dfr = zeros(Nx,Nr);
Ns = round(As/2/dx_ant);
x_filter = (-Ns:Ns)*dx_ant;
for k = 1:Nr
    R = sqrt(x_filter.^2 + r(k).^2);
    filtro = exp(1i*4*pi/lambda*R);
    Dfr(:,k) = conv2(Drc(:,k),filtro(:),'same');
end
%
figure,
imagesc(r,xa,abs(Dfr)), axis xy , title('Focused data')
xlabel('slant range [m]'), ylabel('azimuth [m]')
hold on, plot(r0,x0,'wo')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% EXPECTED SHIFT
expectedShift_x = r0*vr/v
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
