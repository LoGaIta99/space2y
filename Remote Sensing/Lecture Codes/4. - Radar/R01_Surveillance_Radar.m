
% RADAR for aircraft surveillance installed on a rotating platform on the
% ground.

% Evaluate:
% Minimum range,
% Transmitted power, 
% Rotation velocity

clear, clc
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% parameters
Le = 0.5; % [m] antenna size in elevation
La = 3; % [m] antenna size in azimuth
c = 3e8; % [m/s] propagation velocity
f0 = 9e9; % [Hz] central frequency
Ramb = 200e3; % [m] ambiguous range
SNR_dB = 14; % [dB] SNR 
RCS = 10; % [m^2] target RCS 
R = 50e3; % [m] target distance
F_dB = 3; % [dB] noise figure
L_dB = 3; % [dB] atmospheric losses (2way) in dB
duty_cycle = 0.02; % duty cycle
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Rotation velocity
PRI = 2/c*Ramb; % Pulse Repetition Interval
PRF = 1/PRI; % Pulse Repetition Frequency
 
lambda = c/f0;
dphi = lambda/La;
N_angular_sectors = round(2*pi/dphi);
Time_for_one_rotation = N_angular_sectors*PRI
Rot_per_sec = 1/ Time_for_one_rotation % rotations per second
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Minimum range
Tobs = duty_cycle*PRI;
Rmin_m = c/2*Tobs
 
% Power
A = Le*La; % antenna area 
G = 4*pi/lambda^2*A; % antenna gain 
T = 290; % Kelvin, reference noise temperature
K = 1.38e-23; % Boltzmann constant
F = 10^(F_dB/10); % noise figure
N0 = K*T*F; % noise spectral density
L = 10^(L_dB/10); % atmospheric losses over 50 Km
% Pr = Pt*(1/((4*pi*R^2)^2)*G*A*RCS/L)
% Er = Pr*Toss = Pt*(Toss/((4*pi*R^2)^2)*G*A*RCS/L)
% SNR = Er/N0 = Pt*(Toss/((4*pi*R^2)^2)*G*A*RCS/L/N0)
SNR = 10^(SNR_dB/10); % required SNR

Pt_W = SNR/(Tobs/((4*pi*R^2)^2)*G*A*RCS/L/N0) %[Watt]
 
Pt_dBW = 10*log10(Pt_W) % [dBW = dB w.r.t. 1 W]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
