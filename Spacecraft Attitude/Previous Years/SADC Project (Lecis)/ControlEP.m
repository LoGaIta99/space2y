function [sys,x0,str,ts] = controlEP(t,x,u,flag)

% nonlinear pendulum
%
% -------------------------------------------------------------------------


%%
switch flag,

  %%%%%%%%%%%%%%%%%%
  % Initialization %
  %%%%%%%%%%%%%%%%%%
  case 0,
    [sys,x0,str,ts]=mdlInitializeSizes;

  %%%%%%%%%%%%%%%
  % Derivatives %
  %%%%%%%%%%%%%%%
  case 1,
    sys=mdlDerivatives (t,x,u);

  %%%%%%%%%%%
  % Outputs %
  %%%%%%%%%%%
  case 3,
    sys=mdlOutputs (t,x,u);

  %%%%%%%%%%%%%%%%%%%
  % Unhandled flags %
  %%%%%%%%%%%%%%%%%%%
  case { 2, 4, 9 },
    sys = [];

  %%%%%%%%%%%%%%%%%%%%
  % Unexpected flags %
  %%%%%%%%%%%%%%%%%%%%
  otherwise
    error (['Unhandled flag = ',num2str (flag)]);

end


%
%=============================================================================
% mdlInitializeSizes
% Return the sizes, initial conditions, and sample times for the S-function.
%=============================================================================
%
function [sys,x0,str,ts]=mdlInitializeSizes

sizes = simsizes;
sizes.NumContStates  = 0;
sizes.NumDiscStates  = 0;
sizes.NumOutputs     = 3;
sizes.NumInputs      = 12;
sizes.DirFeedthrough = 1;
sizes.NumSampleTimes = 1;

sys = simsizes (sizes);

x0  = [];

str = [];
ts  = [0 0];


%=============================================================================
% mdlDerivatives
% Return the derivatives for the continuous states.
%=============================================================================
%
% function sys=mdlDerivatives (t,x,u)





% end mdlDerivatives

%=============================================================================
% mdlOutputs
% Return the block outputs.
%=============================================================================


function sys=mdlOutputs (t,x,u)
k1=100;
k2=1;

w_BL = [u(1) u(2) u(3)];

A_BL(1,1) = u(4);
A_BL(1,2) = u(5);
A_BL(1,3) = u(6);
A_BL(2,1) = u(7);
A_BL(2,2) = u(8);
A_BL(2,3) = u(9);
A_BL(3,1) = u(10);
A_BL(3,2) = u(11);
A_BL(3,3) = u(12);


Ae1 =(A_BL'-A_BL);
ae_invhatmap = [Ae1(3,2) Ae1(1,3) Ae1(2,1)];

out = - k1*w_BL' - k2*ae_invhatmap';

sys = out;

% end mdlOutputs