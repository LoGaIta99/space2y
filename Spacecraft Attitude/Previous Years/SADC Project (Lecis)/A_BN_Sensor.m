function [sys,x0,str,ts] = A_BN_Sensor(t,x,u,flag)

% Reference sensors
% Magnetometer
% Sun Sensor


%%
switch flag,

  %%%%%%%%%%%%%%%%%%
  % Initialization %
  %%%%%%%%%%%%%%%%%%
  case 0,
    [sys,x0,str,ts]=mdlInitializeSizes;

  %%%%%%%%%%%%%%%
  % Derivatives %
  %%%%%%%%%%%%%%%
  case 1,
    sys=mdlDerivatives (t,x,u);

  %%%%%%%%%%%
  % Outputs %
  %%%%%%%%%%%
  case 3,
    sys=mdlOutputs (t,x,u);

  %%%%%%%%%%%%%%%%%%%
  % Unhandled flags %
  %%%%%%%%%%%%%%%%%%%
  case { 2, 4, 9 },
    sys = [];

  %%%%%%%%%%%%%%%%%%%%
  % Unexpected flags %
  %%%%%%%%%%%%%%%%%%%%
  otherwise
    error (['Unhandled flag = ',num2str (flag)]);

end


%
%=============================================================================
% mdlInitializeSizes
% Return the sizes, initial conditions, and sample times for the S-function.
%=============================================================================
%
function [sys,x0,str,ts]=mdlInitializeSizes

sizes = simsizes;
sizes.NumContStates  = 0;
sizes.NumDiscStates  = 0;
sizes.NumOutputs     = 9;
sizes.NumInputs      = 12;
sizes.DirFeedthrough = 1;
sizes.NumSampleTimes = 1;

sys = simsizes (sizes);

x0  = []';

str = [];
ts  = [0  0];


%=============================================================================
% mdlDerivatives
% Return the derivatives for the continuous states.
%=============================================================================
%
function sys=mdlDerivatives (t,x,u)


sys = [];

% end mdlDerivatives

%=============================================================================
% mdlOutputs
% Return the block outputs.
%=============================================================================


function sys=mdlOutputs (t,x,u)

A_BN = [u(1)   u(2)   u(3)  ;
        u(4)   u(5)   u(6)  ;
        u(7)   u(8)    u(9)]; %input matrix from the kinematics

%random number to evaluate the error
delta1 = u(10);
delta2 = u(11);
delta3 = u(12);

%error matrix
A_err(1,1) = cos(delta1)*cos(delta2);
A_err(1,2) = (sin(delta1)*cos(delta3))+(cos(delta1)*sin(delta2)*sin(delta3));
A_err(1,3) = (sin(delta1)*sin(delta3))-(cos(delta1)*sin(delta2)*cos(delta3));

A_err(2,1) = -sin(delta1)*cos(delta2);
A_err(2,2) = (cos(delta1)*cos(delta3))-(sin(delta1)*sin(delta2)*sin(delta3));
A_err(2,3) = (cos(delta1)*sin(delta3))+(sin(delta1)*sin(delta2)*cos(delta3));

A_err(3,1) = sin(delta2);
A_err(3,2) = -sin(delta3)*cos(delta2);
A_err(3,3) = cos(delta2)*cos(delta3);

%A_BN noisy
A_BN_noisy = A_err*A_BN;


y(1,1) = A_BN_noisy(1,1);  
y(1,2) = A_BN_noisy(1,2); 
y(1,3) = A_BN_noisy(1,3); 

y(1,4) = A_BN_noisy(2,1);  
y(1,5) = A_BN_noisy(2,2);
y(1,6) = A_BN_noisy(2,3);  

y(1,7) = A_BN_noisy(3,1);  
y(1,8) = A_BN_noisy(3,2);  
y(1,9) = A_BN_noisy(3,3); 

sys = [y]; 


% end mdlOutputs