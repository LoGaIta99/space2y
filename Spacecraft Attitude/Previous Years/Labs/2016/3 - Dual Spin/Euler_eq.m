function [sys,x0,str,ts] = Euler_eq(t,x,u,flag)

% nonlinear pendulum
%
% -------------------------------------------------------------------------


%%
switch flag,

  %%%%%%%%%%%%%%%%%%
  % Initialization %
  %%%%%%%%%%%%%%%%%%
  case 0,
    [sys,x0,str,ts]=mdlInitializeSizes;

  %%%%%%%%%%%%%%%
  % Derivatives %
  %%%%%%%%%%%%%%%
  case 1,
    sys=mdlDerivatives (t,x,u);

  %%%%%%%%%%%
  % Outputs %
  %%%%%%%%%%%
  case 3,
    sys=mdlOutputs (t,x,u);

  %%%%%%%%%%%%%%%%%%%
  % Unhandled flags %
  %%%%%%%%%%%%%%%%%%%
  case { 2, 4, 9 },
    sys = [];

  %%%%%%%%%%%%%%%%%%%%
  % Unexpected flags %
  %%%%%%%%%%%%%%%%%%%%
  otherwise
    error (['Unhandled flag = ',num2str (flag)]);

end


%
%=============================================================================
% mdlInitializeSizes
% Return the sizes, initial conditions, and sample times for the S-function.
%=============================================================================
%
function [sys,x0,str,ts]=mdlInitializeSizes

sizes = simsizes;
sizes.NumContStates  = 4;
sizes.NumDiscStates  = 0;
sizes.NumOutputs     = 4;
sizes.NumInputs      = 4;
sizes.DirFeedthrough = 1;
sizes.NumSampleTimes = 1;

sys = simsizes (sizes);

xx0 = [0 0 0 0]';
d0  = [0.01 0.01 0.04  0]'; %change last 
x0 = xx0 +d0;

str = [];
ts  = [0 0];


%=============================================================================
% mdlDerivatives
% Return the derivatives for the continuous states.
%=============================================================================
%
function sys=mdlDerivatives (t,x,u)



%define variables and parameters


I1= 0.0109;
I2= 0.0504;
I3= 0.0506;
Ir= 0.002;

   
%% Equations of the pendulum

c1 = ((I2-I3)/I1);
c2 = ((I3-I1)/I2);
c3 = ((I1-I2)/I3);


%x(1)= 10+0.01;
%x(2)= 0.01;
%x(3)= 0.01;

wdot = zeros(4,1);
wdot(4) = u(4)/Ir;
wdot(1) = c1*x(2)*x(3)-((Ir*x(4)*x(2))/I1);
wdot(2) = c2*x(1)*x(3)+((Ir*x(4)*x(1))/I2);
wdot(3) = c3*x(2)*x(1)-(Ir*wdot(4)/I3);

sys = [wdot];




% end mdlDerivatives

%=============================================================================
% mdlOutputs
% Return the block outputs.
%=============================================================================


function sys=mdlOutputs (t,x,u)

sys = x;


% end mdlOutputs