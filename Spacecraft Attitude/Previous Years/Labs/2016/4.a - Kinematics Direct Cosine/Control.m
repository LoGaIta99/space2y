function [sys,x0,str,ts] = Control(t,x,u,flag)

% nonlinear pendulum
%
% -------------------------------------------------------------------------


%%
switch flag,

  %%%%%%%%%%%%%%%%%%
  % Initialization %
  %%%%%%%%%%%%%%%%%%
  case 0,
    [sys,x0,str,ts]=mdlInitializeSizes;

  %%%%%%%%%%%%%%%
  % Derivatives %
  %%%%%%%%%%%%%%%
  case 1,
    sys=mdlDerivatives (t,x,u);

  %%%%%%%%%%%
  % Outputs %
  %%%%%%%%%%%
  case 3,
    sys=mdlOutputs (t,x,u);

  %%%%%%%%%%%%%%%%%%%
  % Unhandled flags %
  %%%%%%%%%%%%%%%%%%%
  case { 2, 4, 9 },
    sys = [];

  %%%%%%%%%%%%%%%%%%%%
  % Unexpected flags %
  %%%%%%%%%%%%%%%%%%%%
  otherwise
    error (['Unhandled flag = ',num2str (flag)]);

end


%
%=============================================================================
% mdlInitializeSizes
% Return the sizes, initial conditions, and sample times for the S-function.
%=============================================================================
%
function [sys,x0,str,ts]=mdlInitializeSizes

sizes = simsizes;
sizes.NumContStates  = 0;
sizes.NumDiscStates  = 0;
sizes.NumOutputs     = 3;
sizes.NumInputs      = 21;
sizes.DirFeedthrough = 1;
sizes.NumSampleTimes = 1;

sys = simsizes (sizes);
x0=[];



str = [];
ts  = [0 0];


%=============================================================================
% mdlDerivatives
% Return the derivatives for the continuous states.
%=============================================================================
%
function sys=mdlDerivatives (t,x,u)






sys = [];




% end mdlDerivatives

%=============================================================================
% mdlOutputs
% Return the block outputs.
%=============================================================================


function sys=mdlOutputs (t,x,u)
k1 = 1;
k2 = 0.1;
w(1) = u(1);
w(2) = u(2);
w(3) = u(3);
A(1,1) = u(4);
A(1,2) = u(5);
A(1,3) = u(6);
A(2,1) = u(7);
A(2,2) = u(8);
A(2,3) = u(9);
A(3,1) = u(10);
A(3,2) = u(11);
A(3,3) = u(12);
Ad(1,1) = u(13);
Ad(1,2) = u(14);
Ad(1,3) = u(15);
Ad(2,1) = u(16);
Ad(2,2) = u(17);
Ad(2,3) = u(18);
Ad(3,1) = u(19);
Ad(3,2) = u(20);
Ad(3,3) = u(21);
Ae = A*Ad';
Ae1 =(Ae'-Ae);
ae_invhatmap = [Ae1(3,2),Ae1(1,3),Ae1(2,1)]';
out =-k1*w'-k2*ae_invhatmap;
sys = [out];


% end mdlOutputs