function [sys,x0,str,ts] = Kinematics(t,x,u,flag)

% nonlinear pendulum
%
% -------------------------------------------------------------------------


%%
switch flag,

  %%%%%%%%%%%%%%%%%%
  % Initialization %
  %%%%%%%%%%%%%%%%%%
  case 0,
    [sys,x0,str,ts]=mdlInitializeSizes;

  %%%%%%%%%%%%%%%
  % Derivatives %
  %%%%%%%%%%%%%%%
  case 1,
    sys=mdlDerivatives (t,x,u);

  %%%%%%%%%%%
  % Outputs %
  %%%%%%%%%%%
  case 3,
    sys=mdlOutputs (t,x,u);

  %%%%%%%%%%%%%%%%%%%
  % Unhandled flags %
  %%%%%%%%%%%%%%%%%%%
  case { 2, 4, 9 },
    sys = [];

  %%%%%%%%%%%%%%%%%%%%
  % Unexpected flags %
  %%%%%%%%%%%%%%%%%%%%
  otherwise
    error (['Unhandled flag = ',num2str (flag)]);

end


%
%=============================================================================
% mdlInitializeSizes
% Return the sizes, initial conditions, and sample times for the S-function.
%=============================================================================
%
function [sys,x0,str,ts]=mdlInitializeSizes

sizes = simsizes;
sizes.NumContStates  = 9;
sizes.NumDiscStates  = 0;
sizes.NumOutputs     = 9;
sizes.NumInputs      = 3;
sizes.DirFeedthrough = 1;
sizes.NumSampleTimes = 1;

sys = simsizes (sizes);

x0 = [ 1 0 0 0 1 0 0 0 1];  %satellite starting position in direct cosine matrix

str = [];
ts  = [0 0];


%=============================================================================
% mdlDerivatives
% Return the derivatives for the continuous states.
%=============================================================================
%
function sys=mdlDerivatives (t,x,u)

A_BN(1,1) = x(1);
A_BN(1,2) = x(2);
A_BN(1,3) = x(3);
A_BN(2,1) = x(4);
A_BN(2,2) = x(5);
A_BN(2,3) = x(6);
A_BN(3,1) = x(7);
A_BN(3,2) = x(8);
A_BN(3,3) = x(9);

w_BN(1) = u(1);
w_BN(2) = u(2);
w_BN(3) = u(3);

n=1;
w_LN= [0 0 n]';
A_LN = [ cos(n*t) sin(n*t) 0;
        -sin(n*t) cos(n*t) 0;
            0        0     1];
A_BL = A_BN*A_LN';


w_BL = w_BN-A_BL*w_LN;

w_BLhat = [  0   -w_BL(3)   w_BL(2);
          w_BL(3)   0     -w_BL(1);
         -w_BL(2) w_BL(1)     0   ];

   
%s Kinematics of direct cosine in LVLH

A_BLdot = -w_BLhat*A_BL;

aout = vertcat(A_BLdot(1,1:3)', A_BLdot(2,1:3)', A_BLdot(3,1:3)');

sys = [aout];




% end mdlDerivatives

%=============================================================================
% mdlOutputs
% Return the block outputs.
%=============================================================================


function sys=mdlOutputs (t,x,u)

sys = x;


% end mdlOutputs