close all
clear all
set(0,'defaultTextInterpreter','latex')
set(0,'DefaultAxesFontSize', 14);


%% SIMULINK SOLVER SETUP
sim_time = 100
max_dt = .01; %[s]
abs_tol = 1e-7;
rel_tol = 1e-7;
solver_name = 'ode113'

orbit_propagator
set_param('orbit_propagator', 'Solver', solver_name,...
    'MaxStep', num2str(max_dt), 'AbsTol', num2str(abs_tol), 'RelTol', num2str(rel_tol),...
    'StopTime', num2str(sim_time))

%% RUN SIMULINK MODEL
simOut = sim('orbit_propagator')
simOut.getSimulationMetadata.ModelInfo.SolverInfo



%% PLOT
kep = simOut.kep.Data;
theta = wrapTo360(rad2deg( kep(:,6) ));
M_contribs = simOut.M_contribs.Data;

figure
plot(simOut.tout, M_contribs)
legend({'$M_\mathrm{GG}$', '$M_\mathrm{SRP}$', '$M_\mathrm{magn}$'}, 'Interpreter', 'latex')
xlabel('time [s]', 'Interpreter', 'latex')
ylabel('Disturbance Torques [Nm]', 'Interpreter', 'latex')

% figure
% subplot(1,2,1)
% plot(simOut.tout, theta)
% legend({'$\theta$'}, 'Interpreter', 'latex')
% xlabel('time [s]', 'Interpreter', 'latex')
% ylabel('True Anomaly [deg]', 'Interpreter', 'latex')
% 
% 
% subplot(1,2,2)
% sun_angle = simOut.sun_angle.Data;
% sun_angle = wrapTo360(rad2deg( sun_angle ));
% plot(simOut.tout, sun_angle)
% legend({'$\alpha_\mathrm{Sun-SC}$'}, 'Interpreter', 'latex')
% xlabel('time [s]', 'Interpreter', 'latex')
% ylabel('Angle Between Sun and S/C Orientation [deg]', 'Interpreter', 'latex')
