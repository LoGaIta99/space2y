close all
clear all
set(0,'defaultTextInterpreter','latex')
set(0,'DefaultAxesFontSize', 14);


%% SIMULINK SOLVER SETUP
sim_time = 10;
max_dt = .01; %[s]
abs_tol = 1e-7;
rel_tol = 1e-7;
solver_name = 'ode45'

euler_eqs
set_param('euler_eqs', 'Solver', solver_name,...
    'MaxStep', num2str(max_dt), 'AbsTol', num2str(abs_tol), 'RelTol', num2str(rel_tol),...
    'StopTime', num2str(sim_time))

%% RUN SIMULINK MODEL
simOut = sim('euler_eqs')
simOut.getSimulationMetadata.ModelInfo.SolverInfo

%% PLOT
% Numerical Solution
plot(simOut.tout, simOut.omega.Data)
legend({'$\omega_x$', '$\omega_y$', '$\omega_z$'}, 'Interpreter', 'latex')
xlabel('time [s]', 'Interpreter', 'latex')
ylabel('$\omega$ [rad/s]', 'Interpreter', 'latex')

% Analytical Axysimetric Solution (just an APPROX. for our non-symmetric S/C)
% I.C.s: [rad/s]
w_x0 = .45;
w_y0 = .52;
w_z0 = .55;
t_span = [0:.01:sim_time];

omega = zeros(3, length(t_span));
I1 = .7;    I2 = .0504;     I3 = .0109;
lamb = (I3-I1)/I1 * w_z0;
omega(1,:) = w_x0*cos(lamb*t_span) - w_y0*sin(lamb*t_span); 
omega(2,:) = w_x0*sin(lamb*t_span) + w_y0*cos(lamb*t_span);
omega(3,:) = w_z0;
hold on
