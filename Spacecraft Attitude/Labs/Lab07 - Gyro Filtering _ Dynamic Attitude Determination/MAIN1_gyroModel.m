close all
clear all
set(0,'defaultTextInterpreter','latex')
set(0,'DefaultAxesFontSize', 14);


%% SIMULINK SOLVER SETUP
sim_time = 30
max_dt = .01; % [s]
abs_tol = 1e-7;
rel_tol = 1e-7;
solver_name = 'ode113'

gyro_model
set_param('gyro_model', 'Solver', solver_name,...
    'MaxStep', num2str(max_dt), 'AbsTol', num2str(abs_tol), 'RelTol', num2str(rel_tol),...
    'StopTime', num2str(sim_time))



%% RUN SIMULINK MODEL
simOut = sim('gyro_model')
simOut.getSimulationMetadata.ModelInfo.SolverInfo


%% PLOT
t = simOut.tout;
omega = simOut.omega.Data;
omega_raw = simOut.omega_raw.Data;
omega_LPF = simOut.omega_filt.Data;



% Moving Average
f_s = 900;
cutoffFreq = 5 / (2*pi); % [=5rad/s]
numPts = floor((f_s/2)/cutoffFreq)
for k = 1:3
    omega_MA(:,k) = movmean(omega(:,k), [numPts]);
end


h = figure;
set(h, 'Units', 'normalized', 'OuterPosition', [0.13 0.12 0.74 0.8]);
% RAW DATA
subplot(2,1,1)
grid on
hold on
plot(t, omega_raw, 'LineWidth', 1)
plot(t, omega, '--', 'LineWidth', 1)
legend({'$\omega_x^\mathrm{raw}$', '$\omega_y^\mathrm{raw}$', '$\omega_z^\mathrm{raw}$', '$\omega_x^\mathrm{actual}$', '$\omega_y^\mathrm{actual}$', '$\omega_z^\mathrm{actual}$'}, 'Interpreter', 'latex')
xlabel('time [s]', 'Interpreter', 'latex')
ylabel('Angular Velocity [rad/s]', 'Interpreter', 'latex')
title('Raw angular velocity')

% MOVING-AVERAGE-BASED FILTERING
subplot(2,1,2)
grid on
hold on
plot(t, omega_MA)
plot(t, omega, '--', 'LineWidth', 1)
legend({'$\omega_x^\mathrm{MA}$', '$\omega_y^\mathrm{MA}$', '$\omega_z^\mathrm{MA}$', '$\omega_x^\mathrm{actual}$', '$\omega_y^\mathrm{actual}$', '$\omega_z^\mathrm{actual}$'}, 'Interpreter', 'latex')
xlabel('time [s]', 'Interpreter', 'latex')
ylabel('Angular Velocity [rad/s]', 'Interpreter', 'latex')
title('Filtered angular velocity (Moving-Average)')

% LPF APPROACH: suffers delayed signal issue
figure
grid on
hold on
plot(t, omega_LPF)
plot(t, omega, '--', 'LineWidth', 1)
legend({'$\omega_x^\mathrm{LPF}$', '$\omega_y^\mathrm{LPF}$', '$\omega_z^\mathrm{LPF}$', '$\omega_x^\mathrm{actual}$', '$\omega_y^\mathrm{actual}$', '$\omega_z^\mathrm{actual}$'}, 'Interpreter', 'latex')
xlabel('time [s]', 'Interpreter', 'latex')
ylabel('Measured Angular Velocity Error', 'Interpreter', 'latex')


err_raw = omega_raw - omega;
err_LPF = omega_LPF - omega;
err_MA  = omega_MA  - omega;


% ERROR PLOT
h = figure;
set(h, 'Units', 'normalized', 'OuterPosition', [0.13 0.12 0.74 0.8]);
subplot(2,1,1)
grid on
hold on
plot(t, err_raw)
legend({'$\Delta\omega_x^\mathrm{raw}$', '$\Delta\omega_y^\mathrm{raw}$', '$\Delta\omega_z^\mathrm{raw}$'}, 'Interpreter', 'latex')
xlabel('time [s]', 'Interpreter', 'latex')
ylabel('Angular Velocity Error [rad/s]', 'Interpreter', 'latex')
title('Raw data')

subplot(2,1,2)
grid on
hold on
plot(t, err_MA)
legend({'$\Delta\omega_x^\mathrm{MA}$', '$\Delta\omega_y^\mathrm{MA}$', '$\Delta\omega_z^\mathrm{MA}$'}, 'Interpreter', 'latex')
xlabel('time [s]', 'Interpreter', 'latex')
ylabel('Angular Velocity Error [rad/s]', 'Interpreter', 'latex')
title('Filtered data (MA)')

