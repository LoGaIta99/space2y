close all
clear all
set(0,'defaultTextInterpreter','latex')
set(0,'DefaultAxesFontSize', 14);


%% SIMULINK SOLVER SETUP
sim_time = 100;
max_dt = .01; %[s]
abs_tol = 1e-7;
rel_tol = 1e-7;
solver_name = 'ode45'

error_wrt_LVLH
set_param('error_wrt_LVLH', 'Solver', solver_name,...
    'MaxStep', num2str(max_dt), 'AbsTol', num2str(abs_tol), 'RelTol', num2str(rel_tol),...
    'StopTime', num2str(sim_time))

%% RUN SIMULINK MODEL
simOut = sim('error_wrt_LVLH')
simOut.getSimulationMetadata.ModelInfo.SolverInfo



%% PLOT
att_err = simOut.attitude_error.Data;
w_BL = simOut.w_BL.Data;


figure
subplot(2,1,1)
plot(simOut.tout, att_err)
legend({'trace($A_{B/L} - \mathcal{I}$)'}, 'Interpreter', 'latex')
xlabel('time [s]', 'Interpreter', 'latex')
ylabel('Attitude Error', 'Interpreter', 'latex')

subplot(2,1,2)
plot(simOut.tout, w_BL)
legend({'$\omega^{BL}_x$', '$\omega^{BL}_y$', '$\omega^{BL}_z$'}, 'Interpreter', 'latex')
xlabel('time [s]', 'Interpreter', 'latex')
ylabel('$\underline{\omega}^{BL}$ [rad/s]', 'Interpreter', 'latex')
