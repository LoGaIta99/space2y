close all
clear all
set(0,'defaultTextInterpreter','latex')
set(0,'DefaultAxesFontSize', 14);


%% DATA

% ORBITAL MODEL onboard the S/C:
% Magnetometer inertial (i.e. our S/C orbital position):
v1n=[0.2673; 0.5345; 0.8018];
% Sun inertial position:
v2n=[-0.3124; 0.937; 0.1562];


% PERFECT READINGS 
% Magnetometer perfect reading:
vbs1_perf =[0.7749;0.3447;0.5297];
% Sun sensor perfect reading:
vbs2_perf =[0.6295;0.6944;-0.3486];


% READINGS W/ ERRORS
% Magnetometer reading (5 deg error):
vbs1 =[0.7835;0.3690;0.4971];
accuracy_sens1 = 5; % [deg]
% Sun sensor reading (1/8 deg error):
vbs2 =[0.6272;0.6965;-0.3484];
accuracy_sens2 = 1/8; % [deg]



% Actual attitude
A_BN_actual = [0.5335 0.808  0.25;
              -0.808  0.3995 0.433;
               0.25  -0.433  0.866];

  
%% TRIAD
p = vbs1_perf; q = vbs2_perf;
a = v1n; b = v2n;
A_BN_TRIAD_err = TRIAD(p,q, a,b);
TRIAD_error_perfReading = norm(A_BN_actual-A_BN_TRIAD_err)

p = vbs1; q = vbs2;
A_BN_TRIAD = TRIAD(p,q, a,b);
TRIAD_error = norm(A_BN_actual-A_BN_TRIAD)


%% SVD
prec_sens1 = 1/accuracy_sens1;
prec_sens2 = 1/accuracy_sens2;
v_B1 = vbs1_perf; v_B2 = vbs2_perf;
v_N1 = v1n; v_N2 = v2n;
alpha = [prec_sens1 prec_sens2] / (prec_sens1+prec_sens2); % s.t. sum(alpha) = 1

A_BN_SVD = SVD_method(v_B1, v_B2, v_N1, v_N2, alpha(1), alpha(2));
SVD_error_perfReading = norm(A_BN_actual-A_BN_SVD)

v_B1 = vbs1; v_B2 = vbs2;
A_BN_SVD = SVD_method(v_B1, v_B2, v_N1, v_N2, alpha(1), alpha(2));
SVD_error = norm(A_BN_actual-A_BN_SVD)



%% q-Method
% The weight alpha_i identifies sensor precision
% which can be assigned based on its error
prec_sens1 = 1/accuracy_sens1;
prec_sens2 = 1/accuracy_sens2;
v_B1 = vbs1; v_B2 = vbs2;
v_N1 = v1n; v_N2 = v2n;
alpha = [prec_sens1 prec_sens2] / (prec_sens1+prec_sens2); % s.t. sum(alpha) = 1

B = alpha(1)*v_B1*v_N1' + alpha(2)*v_B2*v_N2';

S = B' + B;
z = [B(2,3)-B(3,2); B(3,1)-B(1,3); B(1,2)-B(2,1)];
sigma = trace(B);

K = [(S-sigma*eye(size(S)))   z;
                z'          sigma];
            
[eigenvectors, eigenvalues] = eig(K);
[~,maxIdx] = max(diag(eigenvalues));

q = eigenvectors(:,maxIdx);
q = quatnormalize(q');


A_BN_qMethod = zeros(3,3);
A_BN_qMethod(1,1) = q(1)^2 - q(2)^2 - q(3)^2 + q(4)^2;
A_BN_qMethod(1,2) = 2*(q(2)*q(1) + q(3)*q(4));
A_BN_qMethod(1,3) = 2*(q(1)*q(3) - q(2)*q(4));
A_BN_qMethod(2,1) = 2*(q(2)*q(1) - q(3)*q(4));
A_BN_qMethod(2,2) = -q(1)^2 + q(2)^2 - q(3)^2 + q(4)^2;
A_BN_qMethod(2,3) = 2*(q(3)*q(2) + q(1)*q(4));
A_BN_qMethod(3,1) = 2*(q(1)*q(3) + q(2)*q(4));
A_BN_qMethod(3,2) = 2*(q(2)*q(3) - q(1)*q(4));
A_BN_qMethod(3,3) = -q(1)^2 - q(2)^2 + q(3)^2 + q(4)^2;


qMethod_error = norm(A_BN_actual-A_BN_qMethod)



%% QUEST
B = alpha(1)*v_B1*v_N1' + alpha(2)*v_B2*v_N2';
S = B' + B;
z = [B(2,3)-B(3,2); B(3,1)-B(1,3); B(1,2)-B(2,1)];
sigma = trace(B);

g = (S - sigma*eye(size(S)) - eye(size(S))) \ (-z);



A_BN_QUEST = zeros(3,3);
A_BN_QUEST(1,1) = g(1)^2 - g(2)^2 - g(3)^2 + 1;
A_BN_QUEST(1,2) = 2*(g(2)*g(1) + g(3));
A_BN_QUEST(1,3) = 2*(g(1)*g(3) - g(2));
A_BN_QUEST(2,1) = 2*(g(2)*g(1) - g(3));
A_BN_QUEST(2,2) = -g(1)^2 + g(2)^2 - g(3)^2 + 1;
A_BN_QUEST(2,3) = 2*(g(2)*g(3) + g(1));
A_BN_QUEST(3,1) = 2*(g(1)*g(3) + g(2));
A_BN_QUEST(3,2) = 2*(g(2)*g(3) - g(1));
A_BN_QUEST(3,3) = -g(1)^2 - g(2)^2 + g(3)^2 + 1;
A_BN_QUEST = A_BN_QUEST / (1 + g(1)^2 + g(2)^2 + g(3)^2);


QUEST_error = norm(A_BN_actual-A_BN_QUEST)

