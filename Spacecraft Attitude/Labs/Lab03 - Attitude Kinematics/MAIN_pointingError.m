close all
clear all
set(0,'defaultTextInterpreter','latex')
set(0,'DefaultAxesFontSize', 14);


%% SIMULINK SOLVER SETUP
sim_time = 100;
max_dt = .01; %[s]
abs_tol = 1e-7;
rel_tol = 1e-7;
solver_name = 'ode45'

pointing_error
set_param('pointing_error', 'Solver', solver_name,...
    'MaxStep', num2str(max_dt), 'AbsTol', num2str(abs_tol), 'RelTol', num2str(rel_tol),...
    'StopTime', num2str(sim_time))

%% RUN SIMULINK MODEL
simOut = sim('pointing_error')
simOut.getSimulationMetadata.ModelInfo.SolverInfo

%% PLOT
% Numerical Solution
plot(simOut.tout, simOut.Gamma.Data(:,1:3))
legend({'$\Gamma_x$', '$\Gamma_y$', '$\Gamma_z$'}, 'Interpreter', 'latex')
xlabel('time [s]', 'Interpreter', 'latex')
ylabel('Pointing direction [-]', 'Interpreter', 'latex')

figure
plot(simOut.tout, simOut.theta.Data)
xlabel('time [s]', 'Interpreter', 'latex')
ylabel('$\theta$ [rad]', 'Interpreter', 'latex')
